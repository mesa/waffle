
The Waffle feature release 1.8.0 is now available.

What is new in this release:
  - meson: require meson v0.53, various wayland-scanner fixes
  - cmake: deprecate in favour of meson
  - ci: add lint stage, resolve intermittent Xvfb failures
  - nacl: remove the backend, error out if requested at build
  - cgl: fix warnings and compilation issues
  - wflinfo: add zsh completion, simplify the bash completion
  - tests: rework and de-duplicate the per-platform handling

The source is available at:

  https://waffle.freedesktop.org/files/release/waffle-1.8.0/waffle-1.8.0.tar.xz

You can also get the current source directly from the git
repository. See https://waffle.freedesktop.org for details.

----------------------------------------------------------------

Changes since 1.7.0:

Alexander Kanavin (1):
      meson.build: request native wayland-scanner

Alyssa Ross (1):
      meson: find wayland.xml from wayland-scanner.pc

David Heidelberg (1):
      www: improve layout a bit

Dylan Baker (1):
      meson: use override_dependency and override_find_program

Emil Velikov (73):
      waffle: Bump post-release version to 1.7.90
      doc: Add release notes for 1.7.0
      release: Publish Waffle 1.7.0
      release: Fixup 1.7.0 URLs
      wflinfo: doc: use space separator for longopts
      wflinfo: bash-completion: remove quirky handling
      wflinfo: add zsh completion
      gitlab-ci: use xvfb -noreset
      wayland: silence waffle 1.7 deprecation warnings
      third_party/threads: initialize abs_time
      tests/gl_basic_test: silence maybe-uninitialized warn
      waffle: consistently set WAFFLE_API_VERSION
      waffle: use same version API across programs
      core: use Wpointer-arith safe container_of
      core/tests: correct function type
      wflinfo: fixup sign-compare warnings
      tests/gl_basic_test: silence implicit-fallthrough warn
      tests/gl_basic_test: silence sign-compare warning
      meson: enable all the warnings
      gitlab-ci: flip the error on any warning
      Revert "gbm: work around mesa linkage issue"
      gbm: use libdrm instead of libudev
      clang-format: add config file
      gitlab-ci: add clang-format stage
      gitlab-ci: move the clang-format/git comment further up
      editorconfig: add initial file
      tree: update website references
      doc: update references to new git location
      tree: update maintainer Chad -> Emil
      man: meson: add all dependencies
      meson: omit bash completion, with custom prefix
      www: kill off the manpages
      www: add XDC 2012 presentation notes and videos
      doc: add notes/announce from the website branch
      www: use the in-tree release-notes, tarballs branch
      gitlab-ci: remove trailing whitespace
      gitlab-ci: correctly copy the www root files
      www: add couple more sections to acknowledgements
      editorconfig: https all the links
      README, HACKING: https all the links
      cmake: http -> https where applicable
      doc: http -> https where applicable
      waffle: https all the links
      www: https all the links
      gitlab-ci: correctly handle the release notes
      gitlab-ci: actually use the tarball branch
      gitlab-ci: trigger website updates on .gitlab-ci.yml changes
      gitlab-ci: update container to bullseye
      meson: use get_variable() update to meson 0.51
      meson: remove always true meson.version >= 0.46
      meson: generate cmake files only on Windows
      meson: produce summary(), require meson 0.53
      doc: Add release notes for 1.7.1
      release: Publish 1.7.1
      meson: don't run TLS checks on mingw
      wgl: remove unused dummy wgl_error.[ch]
      all: use format(gnu_printf), enable in mingw
      doc: Add release notes for 1.7.2
      release: Publish 1.7.2
      tests/gl_basic_test: remove #undef test* instances
      tests/gl_basic_test: shuffle expect_error further up
      tests/gl_basic_test: s/test_gl_basic_/test_/
      tests/gl_basic_test: rework rgb tests
      tests/gl_basic_test: rework the ctx flags tests
      tests/gl_basic_test: rework the version tests
      tests/gl_basic_test: print error if waffle_init() fails
      tests/gl_basic_test: rework the testsuite entrypoint
      tests/gl_basic_test: sprinkle some clang-format goodness
      Use a static waffle.def
      meson: drop the libwaffle-1.dll lib prefix on windows
      cmake: deprecate cmake in favour of meson
      Error out when nacl is requested, reinstate WAFFLE_PLATFORM_NACL
      waffle: Bump version to 1.8.0

Fabrice Fontaine (2):
      cmake: drop C++ dependency
      meson: drop C++ dependency

Gert Wollny (2):
      formatting: add add some annotations to not reformat macros
      waffle: Add support for create a context with reset notification

Jason Ekstrand (1):
      wgbm: Don't destroy a surface if init fails

Jose Fonseca (1):
      gitlab-ci: Build and package with MinGW cross-compilers.

Matt Turner (1):
      surfaceless_egl: Implement get_native functions

Philipp Zabel (1):
      wayland: fix build against version 1.20

Romain Naour (1):
      cmake: forward cflags from *.pc files to waffle cflags

Simon McVittie (9):
      waffle_dl: Fix missing format string parameter
      waffle_window_create2: Fix mismatch between format string and parameter
      Mark printf-like functions with the appropriate gcc attribute
      meson: Enable format-string-related compiler warnings
      wflinfo: Print absence of GL context flags correctly
      wflinfo: Make context_flags more const-correct
      meson: Detect non-const-correct uses of string constants
      wflinfo: Move context_flags array into constant data section
      examples, wflinfo: Add missing NORETURN attributes

Simon Zeni (1):
      remove NaCL backend

Yurii Kolesnykov (2):
      Fix build on macOS by fixing a typo
      Add cflag to fix macOS build

