# SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

#
# Summary:
#     Check if the compiler supports various types of thread-local storage.
#
# Usage:
#     include(WaffleCheckThreadLocalStorage)
#     waffle_check_thread_local_storage()
#
# Output Variables:
#     waffle_has_tls
#         True if the compiler supports the `__thread` attribute.
#
#     waffle_has_tls_model_initial_exec
#         True if the compiler supports `__attribute__(tls_model("initial-exec"))`.
#


include(CheckCSourceCompiles)

macro(waffle_check_thread_local_storage)
    # Clang, in its zeal for compatibility with gcc, emits only a warning if
    # it encounters an __attribute__ that it does not recognize. To prevent
    # this function from returning a false positive in that situation, force
    # check_c_source_compiles() to fail on warnings with FAIL_REGEX.

    check_c_source_compiles(
        "
        static __thread int x;

        int
        main() {
            x = 42;
            return x;
        }
        "

        waffle_has_tls

        FAIL_REGEX "warning: |error: "
        )

    check_c_source_compiles(
        "
        static __thread int x __attribute__((tls_model(\"initial-exec\")));

        int
        main() {
            x = 42;
            return x;
        }
        "

        waffle_has_tls_model_initial_exec

        FAIL_REGEX "warning: |error: "
        )
endmacro()
