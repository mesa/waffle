# SPDX-FileCopyrightText: Copyright 2013 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

#
# Waffle's version scheme follows the rules [1] set by the Apache Portable
# Runtime Project.  The scheme allows multiple versions of a library and its
# headers to be installed in parallel.
#
# [1] https://apr.apache.org/versioning.html
#

#
# Release versions.
#
# !!! Must be updated in tandem with the Android.mk !!!
#
# Bump this to x.y.90 immediately after each waffle-x.y.0 release.
#
set(waffle_major_version "1")
set(waffle_minor_version "8")
set(waffle_patch_version "90")

set(waffle_version "${waffle_major_version}.${waffle_minor_version}.${waffle_patch_version}")


#
# Library versions.
#
# On Linux, libwaffle's filename is:
#   libwaffle-MAJOR.so.0.MINOR.PATCH
#
set(waffle_libname "waffle-${waffle_major_version}")
set(waffle_soversion "0")


#
# API version.
#
# This gets bumped to x.(y+1) immediately after each waffle-x.y.0 release.
#
math(EXPR waffle_api_version "(${waffle_major_version} << 8) | ${waffle_minor_version}")
if (waffle_patch_version GREATER 89)
    math(EXPR waffle_api_version "${waffle_api_version} + 1")
endif()
