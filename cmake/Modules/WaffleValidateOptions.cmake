# SPDX-FileCopyrightText: Copyright 2013 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

if(NOT waffle_enable_deprecated_build)
    message(FATAL_ERROR "Building with cmake is deprecated, use meson instead. "
            "If meson does not work for you, please open a bug report and use "
            "cmake -D enable-deprecated-build=1.")
endif()

if(DEFINED waffle_install_includedir)
    message(FATAL_ERROR "Option waffle_install_includedir has been "
            "replaced by CMAKE_INSTALL_INCLUDEDIR. You may need to edit the "
            "CMakeCache to delete the option. See /doc/building.txt for "
            "details.")
endif()

if(DEFINED waffle_install_libdir)
    message(FATAL_ERROR "Option waffle_install_libdir has been "
            "replaced by CMAKE_INSTALL_LIBDIR. You may need to edit the "
            "CMakeCache to delete the option. See /doc/building.txt for "
            "details.")
endif()

if(DEFINED waffle_install_docdir)
    message(FATAL_ERROR "Option waffle_install_docdir has been "
            "replaced by CMAKE_INSTALL_DOCDIR. You may need to edit the "
            "CMakeCache to delete the option. See /doc/building.txt for "
            "details.")
endif()

if(waffle_on_linux)
    if(NOT waffle_has_glx AND NOT waffle_has_wayland AND
       NOT waffle_has_x11_egl AND NOT waffle_has_gbm AND
       NOT waffle_has_surfaceless_egl)
        message(FATAL_ERROR
                "Must enable at least one of: "
                "waffle_has_glx, waffle_has_wayland, "
                "waffle_has_x11_egl, waffle_has_gbm, "
                "waffle_has_surfaceless_egl")
    endif()
    if(waffle_has_gbm)
        if(NOT gbm_FOUND)
            set(gbm_missing_deps
                "${gbm_missing_deps} gbm"
                )
        endif()
        if(NOT libdrm_FOUND)
            set(gbm_missing_deps
                "${gbm_missing_deps} libdrm"
                )
        endif()
        if(NOT egl_FOUND)
            set(gbm_missing_deps
                "${gbm_missing_deps} egl"
                )
        endif()
        if(gbm_missing_deps)
            message(FATAL_ERROR "gbm dependency is missing: ${gbm_missing_deps}")
        endif()
    endif()
    if(waffle_has_glx)
        if(NOT gl_FOUND)
            set(glx_missing_deps
                "${glx_missing_deps} gl"
                )
        endif()
        if(NOT x11-xcb_FOUND)
            set(glx_missing_deps
                "${glx_missing_deps} x11-xcb"
                )
        endif()
        if(glx_missing_deps)
            message(FATAL_ERROR "glx dependency is missing: ${glx_missing_deps}")
        endif()
    endif()
    if(waffle_has_wayland)
        if(NOT wayland-client_FOUND)
            set(wayland_missing_deps
                "${wayland_missing_deps} wayland-client>=1.10"
                )
        endif()
        if(NOT wayland-egl_FOUND)
            set(wayland_missing_deps
                "${wayland_missing_deps} wayland-egl>=9.1"
                )
        endif()
        if(NOT wayland-scanner_FOUND)
            set(wayland_missing_deps
                "${wayland_missing_deps} wayland-scanner>=1.15"
                )
        endif()
        if(NOT wayland-protocols_FOUND)
            set(wayland_missing_deps
                "${wayland_missing_deps} wayland-protocols>=1.12"
                )
        endif()
        if(NOT egl_FOUND)
            set(wayland_missing_deps
                "${wayland_missing_deps} egl"
                )
        endif()
        if(wayland_missing_deps)
            message(FATAL_ERROR "wayland dependency is missing: ${wayland_missing_deps}")
        endif()
    endif()
    if(waffle_has_x11_egl)
        if(NOT x11-xcb_FOUND)
            set(x11_egl_missing_deps
                "${x11_egl_missing_deps} x11-xcb"
                )
        endif()
        if(NOT egl_FOUND)
            set(x11_egl_missing_deps
                "${x11_egl_missing_deps} egl"
                )
        endif()
        if(x11_egl_missing_deps)
            message(FATAL_ERROR "x11_egl dependency is missing: ${x11_egl_missing_deps}")
        endif()
    endif()
    if(waffle_has_surfaceless_egl)
        if(NOT egl_FOUND)
            message(FATAL_ERROR "surfaceless_egl dependency is missing: egl")
        endif()
    endif()
    if(waffle_has_nacl)
        message(FATAL_ERROR "nacl platform is no longer supported. use older"
                            "waffle version, if you depend on it")
    endif()
elseif(waffle_on_mac)
    if(waffle_has_gbm)
        message(FATAL_ERROR "Option is not supported on Darwin: waffle_has_gbm.")
    endif()
    if(waffle_has_glx)
        message(FATAL_ERROR "Option is not supported on Darwin: waffle_has_glx.")
    endif()
    if(waffle_has_wayland)
        message(FATAL_ERROR "Option is not supported on Darwin: waffle_has_wayland.")
    endif()
    if(waffle_has_x11_egl)
        message(FATAL_ERROR "Option is not supported on Darwin: waffle_has_x11_egl.")
    endif()
    if(waffle_has_surfaceless_egl)
        message(FATAL_ERROR "Option is not supported on Darwin: waffle_has_surfaceless_egl.")
    endif()
elseif(waffle_on_windows)
    if(waffle_has_gbm)
        message(FATAL_ERROR "Option is not supported on Windows: waffle_has_gbm.")
    endif()
    if(waffle_has_glx)
        message(FATAL_ERROR "Option is not supported on Windows: waffle_has_glx.")
    endif()
    if(waffle_has_wayland)
        message(FATAL_ERROR "Option is not supported on Windows: waffle_has_wayland.")
    endif()
    if(waffle_has_x11_egl)
        message(FATAL_ERROR "Option is not supported on Windows: waffle_has_x11_egl.")
    endif()
    if(waffle_has_surfaceless_egl)
        message(FATAL_ERROR "Option is not supported on windows: waffle_has_surfaceless_egl.")
    endif()
endif()
