# SPDX-FileCopyrightText: Copyright 2013 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

if("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
    set(waffle_on_linux true)
elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin")
    set(waffle_on_mac true)
elseif("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
    set(waffle_on_windows true)
else()
    message(FATAL_ERROR "Unrecognized CMAKE_SYSTEM_NAME=\"${CMAKE_SYSTEM_NAME}\"")
endif()
