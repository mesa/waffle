# Contributing to waffle

Thanks for taking the time to contribute to waffle!

If you want to submit changes, you can send GitLab [merge requests]. In addition
you can open issues and feature requests on our [GitLab tracker].

In this file you can find:
  * [License and SPDX](#license-and-spdx)
  * [Commit style and history](#commit-style-and-history)
  * [Using commit trailers](#using-commit-trailers)
    + [Signed-off-by](#signed-off-by)
    + [Issues, feature requests](#issues--feature-requests)
    + [Discussions, references](#discussions--references)
    + [Bugfixes, regressions](#bugfixes--regressions)
  * [Coding style](#coding-style)
  * [API manual pages](#api-manual-pages)
  * [Shell completion](#shell-completion)

## License and SPDX

The project uses `BSD-2-Clause` for everything but the manual pages, which are
under `CC BY-SA 3.0 US`. All new changes and additions must be licensed under
the same license.

When adding new files use the respective `SPDX-FileCopyrightText:` and
`SPDX-License-Identifier:` instead of the complete license text.

## Commit style and history

The waffle project uses a [linear, "recipe" style] history. This means that
commits should be small, digestible, stand-alone, and functional.

Commit messages are in imperative mood and merges are to be avoided.

When in doubt, or need a refresher, checking through the output of `git log` is
highly recommended.

## Using commit trailers

Commit messages, apart from stating why a particular change is made, can include
a range of trailers.

### Signed-off-by

By using a `Signed-off-by:` trailer you agree that you comply with the
[Developer Certificate of Origin v1.1], as found in the Linux kernel v3.15.

    Developer's Certificate of Origin 1.1

    By making a contribution to this project, I certify that:

    (a) The contribution was created in whole or in part by me and I
        have the right to submit it under the open source license
        indicated in the file; or

    (b) The contribution is based upon previous work that, to the best
        of my knowledge, is covered under an appropriate open source
        license and I have the right under that license to submit that
        work with modifications, whether created in whole or in part
        by me, under the same open source license (unless I am
        permitted to submit under a different license), as indicated
        in the file; or

    (c) The contribution was provided directly to me by some other
        person who certified (a), (b) or (c) and I have not modified
        it.

    (d) I understand and agree that this project and the contribution
        are public and that a record of the contribution (including all
        personal information I submit with it, including my sign-off) is
        maintained indefinitely and may be redistributed consistent with
        this project or the open source license(s) involved.

### Issues, feature requests

Whenever a patch resolves a particular issue, be that one on our [GitHub
tracker] or elsewhere, use the `Closes:` trailer followed by the full URL.

    Closes: https://gitlab.freedesktop.org/mesa/waffle/-/issues/118

### Discussions, references

If your commit covers a topic raised in an issue, but does not resolve the issue
itself; or otherwise refers to a more complicated topic, you can use
`Reference:`.

### Bugfixes, regressions

Nobody is perfect and regressions happen from time to time. Whenever a commit
addresses a regression caused by another commit, use `Fixes:` as below:

    Fixes: 2c33597 ("wayland: fix build against version 1.20")

## Coding style

The project uses style practically identical to the kernel style. You can see
the in-tree [coding-style file](doc/coding-style.txt) for quick references.

We also have a [.clang-format file](.clang-format) to ease and enforce the
style. Make sure you run `git-clang-format` against your changes, before
submitting MRs.

## API manual pages

Our manual pages are written in xml and we're in the process of converting them
to [scdoc] which uses a simple [markdown-like syntax]. Please make sure to
update them as you add new options to the waffle tools.

## Shell completion

The project provides `bash`, `zsh` and `fish` shell completions for all the
waffle tools. When adding new tools and enhancing existings ones, make sure to
keep them up-to date.

[merge requests]: https://gitlab.freedesktop.org/mesa/waffle/-/merge_requests
[GitLab tracker]: https://gitlab.freedesktop.org/mesa/waffle/-/issues
[linear, "recipe" style]: https://www.bitsnbites.eu/git-history-work-log-vs-recipe/
[scdoc]: https://sr.ht/~sircmpwn/scdoc/
[markdown-like syntax]: https://man.archlinux.org/man/scdoc.5.en
