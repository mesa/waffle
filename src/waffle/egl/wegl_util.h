// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

struct wcore_context;
struct wcore_display;
struct wcore_platform;
struct wcore_window;

struct wegl_platform;

/// @brief Sets the waffle error with info from eglGetError().
/// @param egl_func_call Examples are "eglMakeCurrent()" and
///     "eglBindAPI(EGL_OPENGL_API)".
void
wegl_emit_error(struct wegl_platform *plat, const char *egl_func_call);

bool
wegl_make_current(struct wcore_platform *wc_plat,
                  struct wcore_display *wc_dpy,
                  struct wcore_window *wc_window,
                  struct wcore_context *wc_ctx);

void*
wegl_get_proc_address(struct wcore_platform *wc_self, const char *name);
