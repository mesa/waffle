// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#include "wcore_context.h"
#include "wcore_util.h"

struct wegl_context {
    struct wcore_context wcore;
    EGLContext egl;
};

DEFINE_CONTAINER_CAST_FUNC(wegl_context,
                           struct wegl_context,
                           struct wcore_context,
                           wcore)

bool
wegl_context_init(struct wegl_context *ctx,
                  struct wcore_config *wc_config,
                  struct wcore_context *wc_share_ctx);

bool
wegl_context_teardown(struct wegl_context *ctx);

struct wcore_context*
wegl_context_create(struct wcore_platform *wc_plat,
                    struct wcore_config *wc_config,
                    struct wcore_context *wc_share_ctx);

bool
wegl_context_destroy(struct wcore_context *wc_ctx);
