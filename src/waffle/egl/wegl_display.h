// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#include "wcore_display.h"

struct wcore_display;

enum wegl_supported_api {
    WEGL_OPENGL_API = 1 << 0,
    WEGL_OPENGL_ES_API = 1 << 1,
};

struct wegl_display {
    struct wcore_display wcore;
    EGLDisplay egl;
    enum wegl_supported_api api_mask;
    bool EXT_create_context_robustness;
    bool KHR_create_context;
    bool EXT_image_dma_buf_import_modifiers;
    EGLint major_version;
    EGLint minor_version;
};

DEFINE_CONTAINER_CAST_FUNC(wegl_display,
                           struct wegl_display,
                           struct wcore_display,
                           wcore)

bool
wegl_display_init(struct wegl_display *dpy,
                  struct wcore_platform *wc_plat,
                  void *native_display);

bool
wegl_display_teardown(struct wegl_display *dpy);

bool
wegl_display_supports_context_api(struct wcore_display *wc_dpy,
                                  int32_t waffle_context_api);
