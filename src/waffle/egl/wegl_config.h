// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#include "wcore_config.h"
#include "wcore_util.h"

#include "wegl_display.h"

struct wcore_config_attrs;

struct wegl_config {
    struct wcore_config wcore;
    EGLConfig egl;
    EGLint visual;
};

DEFINE_CONTAINER_CAST_FUNC(wegl_config,
                           struct wegl_config,
                           struct wcore_config,
                           wcore)

struct wcore_config*
wegl_config_choose(struct wcore_platform *wc_plat,
                   struct wcore_display *wc_dpy,
                   const struct wcore_config_attrs *attrs);

bool
wegl_config_destroy(struct wcore_config *wc_config);
