// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "wegl_config.h"
#include "wegl_display.h"
#include "wegl_imports.h"
#include "wegl_platform.h"
#include "wegl_util.h"
#include "wegl_surface.h"

/// On Linux, according to eglplatform.h, EGLNativeDisplayType and intptr_t
/// have the same size regardless of platform.
bool
wegl_window_init(struct wegl_surface *surf,
                 struct wcore_config *wc_config,
                 intptr_t native_window)
{
    struct wegl_config *config = wegl_config(wc_config);
    struct wegl_display *dpy = wegl_display(wc_config->display);
    struct wegl_platform *plat = wegl_platform(dpy->wcore.platform);
    EGLint egl_render_buffer;

    wcore_window_init(&surf->wcore, wc_config);

    if (config->wcore.attrs.double_buffered)
        egl_render_buffer = EGL_BACK_BUFFER;
    else
        egl_render_buffer = EGL_SINGLE_BUFFER;

    EGLint attrib_list[] = {
        EGL_RENDER_BUFFER, egl_render_buffer,
        EGL_NONE,
    };

    surf->egl =
        plat->eglCreateWindowSurface(dpy->egl, config->egl,
                                     (EGLNativeWindowType) native_window,
                                     attrib_list);
    if (!surf->egl) {
        wegl_emit_error(plat, "eglCreateWindowSurface");
        goto fail;
    }

    return true;

fail:
    wegl_surface_teardown(surf);
    return false;
}

bool
wegl_pbuffer_init(struct wegl_surface *surf,
                  struct wcore_config *wc_config,
                  int32_t width, int32_t height)
{
    struct wegl_config *config = wegl_config(wc_config);
    struct wegl_display *dpy = wegl_display(wc_config->display);
    struct wegl_platform *plat = wegl_platform(dpy->wcore.platform);

    wcore_window_init(&surf->wcore, wc_config);

    // Note on pbuffers and double-buffering: The EGL spec says that pbuffers
    // are single-buffered.  But the spec also says that EGL_RENDER_BUFFER is
    // always EGL_BACK_BUFFER for pbuffers. Because EGL is weird in its
    // specification of pbuffers; and because most Piglit tests requests
    // double-buffering (and we don't want to break Piglit); allow creation of
    // pbuffers even if the user requested double-buffering.
    (void) config->wcore.attrs.double_buffered;

    EGLint attrib_list[] = {
        EGL_WIDTH, width,
        EGL_HEIGHT, height,
        EGL_NONE,
    };

    surf->egl = plat->eglCreatePbufferSurface(dpy->egl, config->egl,
                                              attrib_list);
    if (!surf->egl) {
        wegl_emit_error(plat, "eglCreatePbufferSurface");
        goto fail;
    }

    return true;

fail:
    wegl_surface_teardown(surf);
    return false;
}

bool
wegl_surface_teardown(struct wegl_surface *surf)
{
    struct wegl_display *dpy = wegl_display(surf->wcore.display);
    struct wegl_platform *plat = wegl_platform(dpy->wcore.platform);
    bool result = true;

    if (surf->egl) {
        bool ok = plat->eglDestroySurface(dpy->egl, surf->egl);
        if (!ok) {
            wegl_emit_error(plat, "eglDestroySurface");
            result = false;
        }
    }

    return result;
}

bool
wegl_surface_swap_buffers(struct wcore_window *wc_window)
{
    struct wegl_surface *surf = wegl_surface(wc_window);
    struct wegl_display *dpy = wegl_display(surf->wcore.display);
    struct wegl_platform *plat = wegl_platform(dpy->wcore.platform);

    bool ok = plat->eglSwapBuffers(dpy->egl, surf->egl);
    if (!ok)
        wegl_emit_error(plat, "eglSwapBuffers");

    return ok;
}
