// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#include "wcore_window.h"

struct wegl_config;
struct wegl_display;

struct wegl_surface {
    struct wcore_window wcore;
    EGLSurface egl;
};

DEFINE_CONTAINER_CAST_FUNC(wegl_surface,
                           struct wegl_surface,
                           struct wcore_window,
                           wcore)

bool
wegl_window_init(struct wegl_surface *surf,
                 struct wcore_config *wc_config,
                 intptr_t native_window);

bool
wegl_pbuffer_init(struct wegl_surface *surf,
                  struct wcore_config *wc_config,
                  int32_t width, int32_t height);

bool
wegl_surface_teardown(struct wegl_surface *surf);

bool
wegl_surface_swap_buffers(struct wcore_window *wc_window);
