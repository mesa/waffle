// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_window.h"
#include "wcore_util.h"

#include "x11_window.h"

struct wcore_platform;

struct glx_window {
    struct wcore_window wcore;
    struct x11_window x11;
};

DEFINE_CONTAINER_CAST_FUNC(glx_window,
                           struct glx_window,
                           struct wcore_window,
                           wcore)
struct wcore_window*
glx_window_create(struct wcore_platform *wc_plat,
                  struct wcore_config *wc_config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[]);

bool
glx_window_destroy(struct wcore_window *wc_self);

bool
glx_window_show(struct wcore_window *wc_self);

bool
glx_window_resize(struct wcore_window *wc_self,
                  int32_t width, int32_t height);

bool
glx_window_swap_buffers(struct wcore_window *wc_self);

union waffle_native_window*
glx_window_get_native(struct wcore_window *wc_self);
