// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <GL/glx.h>
#include <xcb/xcb.h>

#include "wcore_config.h"
#include "wcore_util.h"

struct wcore_config_attrs;
struct wcore_platform;

struct glx_config {
    struct wcore_config wcore;

    GLXFBConfig glx_fbconfig;
    int32_t glx_fbconfig_id;
    xcb_visualid_t xcb_visual_id;
};

DEFINE_CONTAINER_CAST_FUNC(glx_config,
                           struct glx_config,
                           struct wcore_config,
                           wcore)

struct wcore_config*
glx_config_choose(struct wcore_platform *wc_plat,
                  struct wcore_display *wc_dpy,
                  const struct wcore_config_attrs *attrs);

bool
glx_config_destroy(struct wcore_config *wc_self);

union waffle_native_config*
glx_config_get_native(struct wcore_config *wc_self);
