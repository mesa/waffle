// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdlib.h>
#include <string.h>
#include <xcb/xcb.h>

#include "wcore_attrib_list.h"
#include "wcore_error.h"

#include "glx_config.h"
#include "glx_display.h"
#include "glx_window.h"
#include "glx_wrappers.h"

bool
glx_window_destroy(struct wcore_window *wc_self)
{
    struct glx_window *self = glx_window(wc_self);
    bool ok = x11_window_teardown(&self->x11);

    free(self);
    return ok;
}

struct wcore_window*
glx_window_create(struct wcore_platform *wc_plat,
                  struct wcore_config *wc_config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[])
{
    struct glx_window *self;
    struct glx_display *dpy = glx_display(wc_config->display);
    struct glx_config *config = glx_config(wc_config);
    bool ok = true;

    if (width == -1 && height == -1) {
        width = DisplayWidth(dpy->x11.xlib, dpy->x11.screen);
        height = DisplayHeight(dpy->x11.xlib, dpy->x11.screen);
    }

    if (wcore_attrib_list_length(attrib_list) > 0) {
        wcore_error_bad_attribute(attrib_list[0]);
        return NULL;
    }

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    wcore_window_init(&self->wcore, wc_config);

    ok = x11_window_init(&self->x11,
                         &dpy->x11,
                         config->xcb_visual_id,
                         width,
                         height);
    if (!ok)
        goto error;

    return &self->wcore;

error:
    glx_window_destroy(&self->wcore);
    return NULL;
}

bool
glx_window_show(struct wcore_window *wc_self)
{
    return x11_window_show(&glx_window(wc_self)->x11);
}

bool
glx_window_resize(struct wcore_window *wc_self,
                  int32_t width, int32_t height)
{
    return x11_window_resize(&glx_window(wc_self)->x11, width, height);
}

bool
glx_window_swap_buffers(struct wcore_window *wc_self)
{
    struct glx_window *self = glx_window(wc_self);
    struct glx_display *dpy = glx_display(wc_self->display);
    struct glx_platform *plat = glx_platform(wc_self->display->platform);

    wrapped_glXSwapBuffers(plat, dpy->x11.xlib, self->x11.xcb);

    return true;
}

union waffle_native_window*
glx_window_get_native(struct wcore_window *wc_self)
{
    struct glx_window *self = glx_window(wc_self);
    struct glx_display *dpy = glx_display(wc_self->display);
    union waffle_native_window *n_window;

    WCORE_CREATE_NATIVE_UNION(n_window, glx);
    if (!n_window)
        return NULL;

    n_window->glx->xlib_display = dpy->x11.xlib;
    n_window->glx->xlib_window = self->x11.xcb;

    return n_window;
}
