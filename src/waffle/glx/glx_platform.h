// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <GL/glx.h>
#undef linux

#include "wcore_platform.h"
#include "wcore_util.h"

struct glx_platform {
    struct wcore_platform wcore;

    // glX function pointers
    void *glxHandle;

    GLXContext (*glXCreateNewContext)(Display *dpy, GLXFBConfig config,
                                      int renderType, GLXContext shareList,
                                      Bool direct);
    void (*glXDestroyContext)(Display *dpy, GLXContext ctx);
    Bool (*glXMakeCurrent)(Display *dpy, GLXDrawable drawable, GLXContext ctx);

    const char *(*glXQueryExtensionsString)(Display *dpy, int screen);
    void *(*glXGetProcAddress)(const GLubyte *procname);

    XVisualInfo *(*glXGetVisualFromFBConfig)(Display *dpy, GLXFBConfig config);
    int (*glXGetFBConfigAttrib)(Display *dpy, GLXFBConfig config,
                                int attribute, int *value);
    GLXFBConfig *(*glXChooseFBConfig)(Display *dpy, int screen,
                                      const int *attribList, int *nitems);

    void (*glXSwapBuffers)(Display *dpy, GLXDrawable drawable);


    PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB;
};

DEFINE_CONTAINER_CAST_FUNC(glx_platform,
                           struct glx_platform,
                           struct wcore_platform,
                           wcore)

struct wcore_platform*
glx_platform_create(void);
