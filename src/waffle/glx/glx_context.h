// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include <GL/glx.h>

#include "wcore_config_attrs.h"
#include "wcore_context.h"
#include "wcore_util.h"

struct wcore_config;
struct wcore_platform;

struct glx_context {
    struct wcore_context wcore;
    GLXContext glx;
};

DEFINE_CONTAINER_CAST_FUNC(glx_context,
                           struct glx_context,
                           struct wcore_context,
                           wcore)

struct wcore_context*
glx_context_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   struct wcore_context *wc_share_ctx);

bool
glx_context_destroy(struct wcore_context *wc_self);

union waffle_native_context*
glx_context_get_native(struct wcore_context *wc_self);


// XXX: Keep in sync with glx_config_check_context_attrs()
static inline bool
glx_context_needs_arb_create_context(const struct wcore_config_attrs *attrs)
{
    // Any of the following require the ARB extension, since the data is
    // passed as attributes.

    // Using any GLES* profile,
    if (attrs->context_api != WAFFLE_CONTEXT_OPENGL)
        return true;

    // explicitly requesting OpenGL version (>=3.2),
    if (wcore_config_attrs_version_ge(attrs, 32))
        return true;

    // ... or any of the context flags.
    if (attrs->context_forward_compatible)
        return true;

    if (attrs->context_debug)
        return true;

    if (attrs->context_robust)
        return true;

    return false;
}
