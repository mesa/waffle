// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <GL/glx.h>
#include <X11/Xlib-xcb.h>

#include "waffle_glx.h"

#include "wcore_display.h"
#include "wcore_util.h"

#include "x11_display.h"

struct wcore_platform;

struct glx_display {
    struct wcore_display wcore;
    struct x11_display x11;

    bool ARB_create_context;
    bool ARB_create_context_profile;
    bool ARB_create_context_robustness;
    bool EXT_create_context_es_profile;
    bool EXT_create_context_es2_profile;
};

DEFINE_CONTAINER_CAST_FUNC(glx_display,
                           struct glx_display,
                           struct wcore_display,
                           wcore)

struct wcore_display*
glx_display_connect(struct wcore_platform *wc_plat,
                    const char *name);

bool
glx_display_destroy(struct wcore_display *wc_self);

bool
glx_display_supports_context_api(struct wcore_display *wc_self,
                                 int32_t context_api);

union waffle_native_display*
glx_display_get_native(struct wcore_display *wc_self);
