// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "sl_config.h"

#include "wcore_error.h"

#include "wegl_platform.h"

#include "sl_display.h"

union waffle_native_config *
sl_config_get_native(struct wcore_config *wc_config)
{
    struct sl_display *dpy = sl_display(wegl_display(wc_config->display));
    struct wegl_config *config = wegl_config(wc_config);
    union waffle_native_config *n_config;

    WCORE_CREATE_NATIVE_UNION(n_config, surfaceless_egl);
    if (!n_config)
        return NULL;

    sl_display_fill_native(dpy, &n_config->surfaceless_egl->display);
    n_config->surfaceless_egl->egl_config = config->egl;

    return n_config;
}
