// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "wegl_config.h"

union waffle_native_config;

union waffle_native_config *
sl_config_get_native(struct wcore_config *wc_config);
