// SPDX-FileCopyrightText: Copyright 2016 Google
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wegl_surface.h"

struct wcore_platform;

struct sl_window {
    struct wegl_surface wegl;
    struct wcore_config *wc_config;
};

DEFINE_CONTAINER_CAST_FUNC(sl_window,
                           struct sl_window,
                           struct wegl_surface,
                           wegl)

struct wcore_window*
sl_window_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   int32_t width,
                   int32_t height,
                   const intptr_t attrib_list[]);
bool
sl_window_destroy(struct wcore_window *wc_self);

bool
sl_window_show(struct wcore_window *wc_self);

bool
sl_window_resize(struct wcore_window *wc_self,
                 int32_t width, int32_t height);

union waffle_native_window *
sl_window_get_native(struct wcore_window *wc_self);
