// SPDX-FileCopyrightText: Copyright 2016 Google
// SPDX-License-Identifier: BSD-2-Clause

#include "wcore_error.h"

#include "sl_display.h"
#include "sl_platform.h"

bool
sl_display_destroy(struct wcore_display *wc_self)
{
    struct sl_display *self = sl_display(wegl_display(wc_self));
    bool ok = wegl_display_teardown(&self->wegl);

    free(self);
    return ok;
}

struct wcore_display*
sl_display_connect(struct wcore_platform *wc_plat, const char *name)
{
    struct sl_display *self;
    bool ok = true;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    if (name != NULL) {
        wcore_errorf(WAFFLE_ERROR_BAD_PARAMETER,
                     "parameter 'name' is not NULL");
        goto fail;
    }

    ok = wegl_display_init(&self->wegl, wc_plat, NULL);
    if (!ok)
        goto fail;

    return &self->wegl.wcore;

fail:
    sl_display_destroy(&self->wegl.wcore);
    return NULL;
}

void
sl_display_fill_native(struct sl_display *self,
                       struct waffle_surfaceless_egl_display *n_dpy)
{
    n_dpy->egl_display = self->wegl.egl;
}

union waffle_native_display *
sl_display_get_native(struct wcore_display *wc_self)
{
    struct sl_display *self = sl_display(wegl_display(wc_self));
    union waffle_native_display *n_dpy;

    WCORE_CREATE_NATIVE_UNION(n_dpy, surfaceless_egl);
    if (n_dpy == NULL)
        return NULL;

    sl_display_fill_native(self, n_dpy->surfaceless_egl);

    return n_dpy;
}
