// SPDX-FileCopyrightText: Copyright 2016 Google
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <gbm.h>

#undef linux

#include "wegl_platform.h"
#include "wcore_util.h"

struct sl_platform {
    struct wegl_platform wegl;
};

DEFINE_CONTAINER_CAST_FUNC(sl_platform,
                           struct sl_platform,
                           struct wegl_platform,
                           wegl)

struct wcore_platform *sl_platform_create(void);
