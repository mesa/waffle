// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <dlfcn.h>
#include <stdlib.h>

#include "wcore_error.h"

#include "posix_platform.h"

#include "wegl_config.h"
#include "wegl_context.h"
#include "wegl_platform.h"
#include "wegl_util.h"

#include "sl_config.h"
#include "sl_display.h"
#include "sl_platform.h"
#include "sl_window.h"

static const struct wcore_platform_vtbl sl_platform_vtbl;

static bool
sl_platform_destroy(struct wcore_platform *wc_self)
{
    struct sl_platform *self = sl_platform(wegl_platform(wc_self));
    bool ok = wegl_platform_teardown(&self->wegl);

    free(self);
    return ok;
}

struct wcore_platform*
sl_platform_create(void)
{
    struct sl_platform *self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    bool ok = wegl_platform_init(&self->wegl, EGL_PLATFORM_SURFACELESS_MESA);
    if (!ok)
        goto fail;

    self->wegl.egl_surface_type_mask = EGL_PBUFFER_BIT;

    self->wegl.wcore.vtbl = &sl_platform_vtbl;
    return &self->wegl.wcore;

fail:
    sl_platform_destroy(&self->wegl.wcore);
    return NULL;
}

static union waffle_native_context *
sl_context_get_native(struct wcore_context *wc_ctx)
{
    struct sl_display *dpy = sl_display(wegl_display(wc_ctx->display));
    struct wegl_context *ctx = wegl_context(wc_ctx);
    union waffle_native_context *n_ctx;

    WCORE_CREATE_NATIVE_UNION(n_ctx, surfaceless_egl);
    if (!n_ctx)
        return NULL;

    sl_display_fill_native(dpy, &n_ctx->surfaceless_egl->display);
    n_ctx->surfaceless_egl->egl_context = ctx->egl;

    return n_ctx;
}

static const struct wcore_platform_vtbl sl_platform_vtbl = {
    .destroy = sl_platform_destroy,

    .make_current = wegl_make_current,
    .get_proc_address = wegl_get_proc_address,

    .dl_can_open = posix_platform_can_dlopen,
    .dl_sym = posix_platform_dlsym,

    .display = {
        .connect = sl_display_connect,
        .destroy = sl_display_destroy,
        .supports_context_api = wegl_display_supports_context_api,
        .get_native = sl_display_get_native,
    },

    .config = {
        .choose = wegl_config_choose,
        .destroy = wegl_config_destroy,
        .get_native = sl_config_get_native,
    },

    .context = {
        .create = wegl_context_create,
        .destroy = wegl_context_destroy,
        .get_native = sl_context_get_native,
    },

    .window = {
        .create = sl_window_create,
        .destroy = sl_window_destroy,
        .show = sl_window_show,
        .resize = sl_window_resize,
        .swap_buffers = wegl_surface_swap_buffers,
        .get_native = sl_window_get_native,
    },
};
