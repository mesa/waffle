// SPDX-FileCopyrightText: Copyright 2016 Google
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "waffle_surfaceless_egl.h"

#include "wegl_display.h"

struct sl_display {
    struct wegl_display wegl;
};

DEFINE_CONTAINER_CAST_FUNC(sl_display,
                           struct sl_display,
                           struct wegl_display,
                           wegl)

struct wcore_display*
sl_display_connect(struct wcore_platform *wc_plat, const char *name);

bool
sl_display_destroy(struct wcore_display *wc_self);

union waffle_native_display *
sl_display_get_native(struct wcore_display *wc_self);

void
sl_display_fill_native(struct sl_display *self,
                       struct waffle_surfaceless_egl_display *n_dpy);
