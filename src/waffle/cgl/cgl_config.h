// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <OpenGL/OpenGL.h>

#include "wcore_config.h"
#include "wcore_util.h"

struct wcore_config_attrs;
struct wcore_platform;

struct cgl_config {
    struct wcore_config wcore;
    CGLPixelFormatObj pixel_format;
};

DEFINE_CONTAINER_CAST_FUNC(cgl_config,
                           struct cgl_config,
                           struct wcore_config,
                           wcore)

struct wcore_config*
cgl_config_choose(struct wcore_platform *wc_plat,
                  struct wcore_display *wc_dpy,
                  const struct wcore_config_attrs *attrs);

bool
cgl_config_destroy(struct wcore_config *wc_self);
