// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "wcore_platform.h"
#include "wcore_util.h"

struct cgl_platform {
    struct wcore_platform wcore;

    int32_t system_version_major;
    int32_t system_version_minor;

    /// @brief Has value (major << 8) | minor.
    ///
    /// For example, 0x0a09 indicates OS X 10.9 Mavericks.
    int32_t system_version_full;
};

DEFINE_CONTAINER_CAST_FUNC(cgl_platform,
                           struct cgl_platform,
                           struct wcore_platform,
                           wcore)

struct wcore_platform*
cgl_platform_create(void);
