// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "WaffleGLView.h"

@implementation WaffleGLView

    @synthesize glContext = _glContext;

    - (BOOL) isOpaque {
        return YES;
    }

    - (BOOL) canBecomeKeyView {
        return YES;
    }

    - (BOOL) acceptsFirstResponder {
        return YES;
    }

    - (void) swapBuffers {
        [_glContext flushBuffer];
    }

    - (void) drawRect: (NSRect) bounds {
        [self swapBuffers];
    }
@end
