// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include <Cocoa/Cocoa.h>
#include <OpenGL/OpenGL.h>

#include "wcore_context.h"
#include "wcore_util.h"

struct wcore_config;
struct wcore_platform;

struct cgl_context {
    struct wcore_context wcore;
    NSOpenGLContext *ns;
};

DEFINE_CONTAINER_CAST_FUNC(cgl_context,
                           struct cgl_context,
                           struct wcore_context,
                           wcore)

struct wcore_context*
cgl_context_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   struct wcore_context *wc_share_ctx);

bool
cgl_context_destroy(struct wcore_context *wc_self);
