// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include <OpenGL/OpenGL.h>

#include "wcore_error.h"

#include "cgl_error.h"

const char*
cgl_error_to_string(int error_code)
{
    switch (error_code) {
        #define CASE(x) case x: return #x
        CASE(kCGLNoError);
        CASE(kCGLBadAttribute);
        CASE(kCGLBadProperty);
        CASE(kCGLBadPixelFormat);
        CASE(kCGLBadRendererInfo);
        CASE(kCGLBadContext);
        CASE(kCGLBadDrawable);
        CASE(kCGLBadDisplay);
        CASE(kCGLBadState);
        CASE(kCGLBadValue);
        CASE(kCGLBadMatch);
        CASE(kCGLBadEnumeration);
        CASE(kCGLBadOffScreen);
        CASE(kCGLBadFullScreen);
        CASE(kCGLBadWindow);
        CASE(kCGLBadAddress);
        CASE(kCGLBadCodeModule);
        CASE(kCGLBadAlloc);
        CASE(kCGLBadConnection);
        #undef CASE

        default:
            assert(false);
            return NULL;
    }
}

void
cgl_error_failed_func(const char *func_name, int error_code)
{
    wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                 "%s failed with %s: %s",
                 func_name,
                 cgl_error_to_string(error_code),
                 CGLErrorString(error_code));
}
