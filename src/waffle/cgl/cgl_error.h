// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

const char*
cgl_error_to_string(int error_code);

void
cgl_error_failed_func(const char *func_name, int error_code);
