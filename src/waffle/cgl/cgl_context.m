// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <assert.h>
#include <stdlib.h>

#include "wcore_error.h"

#include "cgl_config.h"
#include "cgl_context.h"
#include "cgl_error.h"

bool
cgl_context_destroy(struct wcore_context *wc_self)
{
    struct cgl_context *self = cgl_context(wc_self);

    [self->ns release];
    free(self);
    return true;
}

struct wcore_context*
cgl_context_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   struct wcore_context *wc_share_ctx)
{
    struct cgl_context *self;
    struct cgl_config *config = cgl_config(wc_config);
    struct cgl_context *share_ctx = cgl_context(wc_share_ctx);

    CGLContextObj cgl_self = NULL;
    CGLContextObj cgl_share_ctx = NULL;

    self = wcore_calloc(sizeof(*self));
    if (!self)
        return NULL;

    wcore_context_init(&self->wcore, wc_config);

    if (share_ctx)
        cgl_share_ctx = [share_ctx->ns CGLContextObj];

    int error =
        CGLCreateContext(config->pixel_format, cgl_share_ctx, &cgl_self);
    if (error) {
        cgl_error_failed_func("CGLCreateContext", error);
        goto fail;
    }

    self->ns = [[NSOpenGLContext alloc] initWithCGLContextObj:cgl_self];
    if (!self->ns) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                     "NSOpenGLContext.initWithCGLContextObj failed");
        goto fail;
    }

    // The NSOpenGLContext now owns the CGLContext.
    CGLReleaseContext(cgl_self);

    return &self->wcore;

fail:
    cgl_context_destroy(&self->wcore);
    return NULL;
}
