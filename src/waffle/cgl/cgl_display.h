// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "wcore_display.h"
#include "wcore_util.h"

struct wcore_platform;

struct cgl_display {
    struct wcore_display wcore;
};

DEFINE_CONTAINER_CAST_FUNC(cgl_display,
                           struct cgl_display,
                           struct wcore_display,
                           wcore)

struct wcore_display*
cgl_display_connect(struct wcore_platform *wc_plat,
                    const char *name);

bool
cgl_display_destroy(struct wcore_display *wc_self);

bool
cgl_display_supports_context_api(struct wcore_display *wc_self,
                                 int32_t context_api);
