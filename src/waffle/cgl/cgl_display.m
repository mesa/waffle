// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <assert.h>
#include <stdlib.h>

#include "wcore_error.h"

#include "cgl_display.h"

bool
cgl_display_destroy(struct wcore_display *wc_self)
{
    struct cgl_display *self = cgl_display(wc_self);

    free(self);
    return true;
}


struct wcore_display*
cgl_display_connect(struct wcore_platform *wc_plat,
                    const char *name)
{
    struct cgl_display *self;

    self = wcore_calloc(sizeof(*self));
    if (!self)
        return NULL;

    wcore_display_init(&self->wcore, wc_plat);

    return &self->wcore;

error:
    cgl_display_destroy(&self->wcore);
    return NULL;
}

bool
cgl_display_supports_context_api(struct wcore_display *wc_self,
                                 int32_t context_api)
{
    switch (context_api) {
        case WAFFLE_CONTEXT_OPENGL:
            return true;
        case WAFFLE_CONTEXT_OPENGL_ES1:
        case WAFFLE_CONTEXT_OPENGL_ES2:
        case WAFFLE_CONTEXT_OPENGL_ES3:
            return false;
        default:
            assert(false);
            return false;
    }
}
