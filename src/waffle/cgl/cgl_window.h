// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_util.h"
#include "wcore_window.h"

#include "WaffleGLView.h"

struct wcore_platform;

struct cgl_window {
    struct wcore_window wcore;

    NSWindow *ns_window;
    WaffleGLView *gl_view;
};

DEFINE_CONTAINER_CAST_FUNC(cgl_window,
                           struct cgl_window,
                           struct wcore_window,
                           wcore)
struct wcore_window*
cgl_window_create(struct wcore_platform *wc_plat,
                  struct wcore_config *wc_config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[]);

bool
cgl_window_destroy(struct wcore_window *wc_self);

bool
cgl_window_show(struct wcore_window *wc_self);

bool
cgl_window_resize(struct wcore_window *wc_self,
                  int32_t width, int32_t height);

bool
cgl_window_swap_buffers(struct wcore_window *wc_self);

union waffle_native_window*
cgl_window_get_native(struct wcore_window *wc_self);
