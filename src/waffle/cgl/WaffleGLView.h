// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

@interface WaffleGLView : NSView {

    @private
        NSOpenGLContext* _glContext;
    }

    @property (readwrite, retain) NSOpenGLContext* glContext;

    - (void) swapBuffers;
    - (void) drawRect: (NSRect) bounds;
    - (BOOL) isOpaque;
    - (BOOL) canBecomeKeyView;
    - (BOOL) acceptsFirstResponder;
@end
