// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

#include "wcore_attrib_list.h"
#include "wcore_error.h"

#include "cgl_config.h"
#include "cgl_window.h"

bool
cgl_window_destroy(struct wcore_window *wc_self)
{
    struct cgl_window *self = cgl_window(wc_self);

    if (self->gl_view)
        [self->gl_view release];

    if (self->ns_window)
        [self->ns_window release];

    free(self);
    return true;
}


static WaffleGLView*
cgl_window_create_gl_view(int32_t width, int32_t height)
{
    WaffleGLView *view = [[WaffleGLView alloc]
                          initWithFrame:NSMakeRect(0, 0, width, height)];

    if (!view)
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "failed to create NSView");

    return view;
}

static NSWindow*
cgl_window_create_ns_window(NSView *view)
{
    // WARNING(chad): I understand neither Objective-C nor Cocoa. This is
    // WARNING(chad): likely all wrong.
    int styleMask = NSTitledWindowMask | NSClosableWindowMask;

    NSWindow *window = [[NSWindow alloc]
                        initWithContentRect:[view frame]
                                  styleMask:styleMask
                                    backing:NSBackingStoreBuffered
                                      defer:NO];

    if (!window) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "failed to create NSWindow");
        return NULL;
    }

    [window setContentView:view];
    [window makeFirstResponder:view];
    [window center];
    [window makeKeyAndOrderFront:nil];
    [window orderFrontRegardless];

    return window;
}

struct wcore_window*
cgl_window_create(struct wcore_platform *wc_plat,
                  struct wcore_config *wc_config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[])
{
    struct cgl_window *self;

    if (width == -1 && height == -1) {
        wcore_errorf(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM,
                     "fullscreen window not supported");
        return NULL;
    }

    if (wcore_attrib_list_length(attrib_list) > 0) {
        wcore_error_bad_attribute(attrib_list[0]);
        return NULL;
    }

    self = wcore_calloc(sizeof(*self));
    if (!self)
        return NULL;

    wcore_window_init(&self->wcore, wc_config);

    self->gl_view = cgl_window_create_gl_view(width, height);
    if (!self->gl_view)
        goto error;

    self->ns_window = cgl_window_create_ns_window(self->gl_view);
    if (!self->ns_window)
        goto error;

    return &self->wcore;

error:
    cgl_window_destroy(&self->wcore);
    return NULL;
}

bool
cgl_window_show(struct wcore_window *wc_self)
{
    return true;
}

bool
cgl_window_resize(struct wcore_window *wc_self,
                  int32_t width, int32_t height)
{
    struct cgl_window *self = cgl_window(wc_self);
    [self->ns_window setContentSize:(NSSize){width, height}];
    return true;
}

bool
cgl_window_swap_buffers(struct wcore_window *wc_self)
{
    struct cgl_window *self = cgl_window(wc_self);
    [self->gl_view swapBuffers];
    return true;

}

union waffle_native_window*
cgl_window_get_native(struct wcore_window *wc_self)
{
    wcore_error(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM);
    return NULL;
}
