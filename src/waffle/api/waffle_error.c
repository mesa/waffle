// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <string.h>

#include "api_priv.h"

#include "wcore_error.h"

WAFFLE_API enum waffle_error
waffle_error_get_code(void)
{
    return wcore_error_get_code();
}

WAFFLE_API const struct waffle_error_info*
waffle_error_get_info(void)
{
    return wcore_error_get_info();
}

WAFFLE_API const char*
waffle_error_to_string(enum waffle_error e)
{
    switch (e) {
#define CASE(x) case x: return #x
        CASE(WAFFLE_NO_ERROR);
        CASE(WAFFLE_ERROR_FATAL);
        CASE(WAFFLE_ERROR_UNKNOWN);
        CASE(WAFFLE_ERROR_INTERNAL);
        CASE(WAFFLE_ERROR_BAD_ALLOC);
        CASE(WAFFLE_ERROR_NOT_INITIALIZED);
        CASE(WAFFLE_ERROR_ALREADY_INITIALIZED);
        CASE(WAFFLE_ERROR_BAD_ATTRIBUTE);
        CASE(WAFFLE_ERROR_BAD_PARAMETER);
        CASE(WAFFLE_ERROR_BAD_DISPLAY_MATCH);
        CASE(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM);
        CASE(WAFFLE_ERROR_BUILT_WITHOUT_SUPPORT);
        default: return 0;
#undef CASE
    }
}
