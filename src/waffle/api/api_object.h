// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "waffle.h"

#ifdef __cplusplus
extern "C" {
#endif

// This header is so sad and lonely... but there is no other appropriate place
// to define this struct.

struct api_object {
    /// @brief Display to which object belongs.
    ///
    /// For consistency, a `waffle_display` belongs to itself.
    size_t display_id;
};

#ifdef __cplusplus
}
#endif
