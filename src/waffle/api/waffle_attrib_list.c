// SPDX-FileCopyrightText: Copyright 2013 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "api_priv.h"

#include "wcore_attrib_list.h"
#include "wcore_error.h"

WAFFLE_API int32_t
waffle_attrib_list_length(const int32_t attrib_list[])
{

    wcore_error_reset();
    return wcore_attrib_list32_length(attrib_list);
}

WAFFLE_API bool
waffle_attrib_list_get(
        const int32_t *attrib_list,
        int32_t key,
        int32_t *value)
{
    wcore_error_reset();
    return wcore_attrib_list32_get(attrib_list, key, value);
}

WAFFLE_API bool
waffle_attrib_list_get_with_default(
        const int32_t attrib_list[],
        int32_t key,
        int32_t *value,
        int32_t default_value)
{
    wcore_error_reset();
    return wcore_attrib_list32_get_with_default(attrib_list, key, value,
                                               default_value);
}

WAFFLE_API bool
waffle_attrib_list_update(
        int32_t *attrib_list,
        int32_t key,
        int32_t value)
{
    wcore_error_reset();
    return wcore_attrib_list32_update(attrib_list, key, value);
}
