// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "api_priv.h"

#include "wcore_config_attrs.h"
#include "wcore_config.h"
#include "wcore_display.h"
#include "wcore_error.h"
#include "wcore_platform.h"

WAFFLE_API struct waffle_config*
waffle_config_choose(
        struct waffle_display *dpy,
        const int32_t attrib_list[])
{
    struct wcore_config *wc_self;
    struct wcore_display *wc_dpy = wcore_display(dpy);
    struct wcore_config_attrs attrs;
    bool ok = true;

    const struct api_object *obj_list[] = {
        wc_dpy ? &wc_dpy->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return NULL;

    ok = wcore_config_attrs_parse(attrib_list, &attrs);
    if (!ok)
        return NULL;

    wc_self = api_platform->vtbl->config.choose(api_platform, wc_dpy, &attrs);
    if (!wc_self)
        return NULL;

    return waffle_config(wc_self);
}

WAFFLE_API bool
waffle_config_destroy(struct waffle_config *self)
{
    struct wcore_config *wc_self = wcore_config(self);

    const struct api_object *obj_list[] = {
        wc_self ? &wc_self->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return false;

    return api_platform->vtbl->config.destroy(wc_self);
}

WAFFLE_API union waffle_native_config*
waffle_config_get_native(struct waffle_config *self)
{
    struct wcore_config *wc_self = wcore_config(self);

    const struct api_object *obj_list[] = {
        wc_self ? &wc_self->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return NULL;

    if (api_platform->vtbl->config.get_native) {
        return api_platform->vtbl->config.get_native(wc_self);
    }
    else {
        wcore_error(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM);
        return NULL;
    }
}
