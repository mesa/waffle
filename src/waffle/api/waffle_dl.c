// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "api_priv.h"

#include "wcore_error.h"
#include "wcore_platform.h"

static bool
waffle_dl_check_enum(int32_t dl)
{
    switch (dl) {
        case WAFFLE_DL_OPENGL:
        case WAFFLE_DL_OPENGL_ES1:
        case WAFFLE_DL_OPENGL_ES2:
        case WAFFLE_DL_OPENGL_ES3:
            return true;
        default:
            wcore_errorf(WAFFLE_ERROR_BAD_PARAMETER, "dl has bad value %#x",
                         dl);
            return false;
    }
}

WAFFLE_API bool
waffle_dl_can_open(int32_t dl)
{
    if (!api_check_entry(NULL, 0))
         return false;

     if (!waffle_dl_check_enum(dl))
         return false;

     return api_platform->vtbl->dl_can_open(api_platform, dl);
}

WAFFLE_API void*
waffle_dl_sym(int32_t dl, const char *name)
{
    if (!api_check_entry(NULL, 0))
        return NULL;

    if (!waffle_dl_check_enum(dl))
        return NULL;

    return api_platform->vtbl->dl_sym(api_platform, dl, name);
}
