// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "api_priv.h"

#include "wcore_error.h"
#include "wcore_display.h"
#include "wcore_platform.h"
#include "wcore_util.h"

WAFFLE_API struct waffle_display*
waffle_display_connect(const char *name)
{
    struct wcore_display *wc_self;

    if (!api_check_entry(NULL, 0))
        return NULL;

    wc_self = api_platform->vtbl->display.connect(api_platform, name);
    if (!wc_self)
        return NULL;

    return waffle_display(wc_self);
}

WAFFLE_API bool
waffle_display_disconnect(struct waffle_display *self)
{
    struct wcore_display *wc_self = wcore_display(self);

    const struct api_object *obj_list[] = {
        wc_self ? &wc_self->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return false;

    return api_platform->vtbl->display.destroy(wc_self);
}

WAFFLE_API bool
waffle_display_supports_context_api(
        struct waffle_display *self,
        int32_t context_api)
{
    struct wcore_display *wc_self = wcore_display(self);

    const struct api_object *obj_list[] = {
        wc_self ? &wc_self->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return false;

    switch (context_api) {
        case WAFFLE_CONTEXT_OPENGL:
        case WAFFLE_CONTEXT_OPENGL_ES1:
        case WAFFLE_CONTEXT_OPENGL_ES2:
        case WAFFLE_CONTEXT_OPENGL_ES3:
            break;
        default:
            wcore_errorf(WAFFLE_ERROR_BAD_PARAMETER,
                         "context_api has bad value %#x", context_api);
            return false;
    }

    return api_platform->vtbl->display.supports_context_api(wc_self,
                                                            context_api);
}

WAFFLE_API union waffle_native_display*
waffle_display_get_native(struct waffle_display *self)
{
    struct wcore_display *wc_self = wcore_display(self);

    const struct api_object *obj_list[] = {
        wc_self ? &wc_self->api : NULL,
    };

    if (!api_check_entry(obj_list, 1))
        return NULL;

    if (api_platform->vtbl->display.get_native) {
        return api_platform->vtbl->display.get_native(wc_self);
    }
    else {
        wcore_error(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM);
        return NULL;
    }
}
