// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "api_priv.h"

#include "wcore_error.h"
#include "wcore_util.h"

WAFFLE_API const char*
waffle_enum_to_string(int32_t e)
{
    wcore_error_reset();
    return wcore_enum_to_string(e);
}
