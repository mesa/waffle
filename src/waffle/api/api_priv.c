// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdio.h>
#include <stdlib.h>

#include "api_object.h"
#include "api_priv.h"

#include "wcore_error.h"
#include "wcore_platform.h"

struct wcore_platform *api_platform = 0;

bool
api_check_entry(const struct api_object *obj_list[], int length)
{
    wcore_error_reset();

    if (!api_platform) {
        wcore_error(WAFFLE_ERROR_NOT_INITIALIZED);
        return false;
    }

    for (int i = 0; i < length; ++i) {
        if (obj_list[i] == NULL) {
            wcore_errorf(WAFFLE_ERROR_BAD_PARAMETER, "null pointer");
            return false;
        }

        if (obj_list[i]->display_id != obj_list[0]->display_id) {
            wcore_error(WAFFLE_ERROR_BAD_DISPLAY_MATCH);
            return false;
        }
    }

    return true;
}
