// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wegl_surface.h"

struct wcore_platform;
struct gbm_surface;

struct wgbm_window {
    struct gbm_surface *gbm_surface;
    struct wegl_surface wegl;
    struct wcore_config *wc_config;
};

static inline struct wgbm_window*
wgbm_window(struct wcore_window *wc_self)
{
    if (wc_self) {
        struct wegl_surface *wegl_self = container_of(wc_self, struct wegl_surface, wcore);
        return container_of(wegl_self, struct wgbm_window, wegl);
    }
    else {
        return NULL;
    }
}

struct wcore_window*
wgbm_window_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   int32_t width,
                   int32_t height,
                   const intptr_t attrib_list[]);

bool
wgbm_window_destroy(struct wcore_window *wc_self);

bool
wgbm_window_show(struct wcore_window *wc_self);

bool
wgbm_window_swap_buffers(struct wcore_window *wc_self);

bool
wgbm_window_resize(struct wcore_window *wc_self,
                   int32_t width, int32_t height);

union waffle_native_window*
wgbm_window_get_native(struct wcore_window *wc_self);
