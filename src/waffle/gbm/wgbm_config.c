// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include "wcore_error.h"

#include "wegl_platform.h"

#include "wgbm_config.h"
#include "wgbm_display.h"

union waffle_native_config*
wgbm_config_get_native(struct wcore_config *wc_config)
{
    struct wgbm_display *dpy = wgbm_display(wc_config->display);
    struct wegl_config *config = wegl_config(wc_config);
    union waffle_native_config *n_config;

    WCORE_CREATE_NATIVE_UNION(n_config, gbm);
    if (!n_config)
        return NULL;

    wgbm_display_fill_native(dpy, &n_config->gbm->display);
    n_config->gbm->egl_config = config->egl;

    return n_config;
}
