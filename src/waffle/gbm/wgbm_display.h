// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "waffle_gbm.h"

#include "wegl_display.h"

struct wcore_platform;
struct gbm_device;

struct wgbm_display {
    struct gbm_device *gbm_device;
    struct wegl_display wegl;
};

static inline struct wgbm_display*
wgbm_display(struct wcore_display *wc_self)
{
    if (wc_self) {
        struct wegl_display *wegl_self = container_of(wc_self, struct wegl_display, wcore);
        return container_of(wegl_self, struct wgbm_display, wegl);
    }
    else {
        return NULL;
    }
}

struct wcore_display*
wgbm_display_connect(struct wcore_platform *wc_plat,
                     const char *name);

bool
wgbm_display_destroy(struct wcore_display *wc_self);

union waffle_native_display*
wgbm_display_get_native(struct wcore_display *wc_self);

void
wgbm_display_fill_native(struct wgbm_display *self,
                         struct waffle_gbm_display *n_dpy);
