// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// @brief Encodes the attribute list received by waffle_config_choose().
struct wcore_config_attrs {
    int32_t context_api;
    int32_t context_major_version;
    int32_t context_minor_version;
    int32_t context_profile;

    int32_t rgb_size;
    int32_t rgba_size;

    int32_t red_size;
    int32_t green_size;
    int32_t blue_size;
    int32_t alpha_size;

    int32_t depth_size;
    int32_t stencil_size;

    int32_t samples;

    bool context_forward_compatible;
    bool context_debug;
    bool context_robust;
    bool double_buffered;
    bool sample_buffers;
    bool accum_buffer;
    bool lose_context_on_reset;
};

bool
wcore_config_attrs_parse(
      const int32_t waffle_attrib_list[],
      struct wcore_config_attrs *attrs);

bool
wcore_config_attrs_version_eq(
      const struct wcore_config_attrs *attrs,
      int merged_version);

bool
wcore_config_attrs_version_gt(
      const struct wcore_config_attrs *attrs,
      int merged_version);

bool
wcore_config_attrs_version_ge(
      const struct wcore_config_attrs *attrs,
      int merged_version);

bool
wcore_config_attrs_version_lt(
      const struct wcore_config_attrs *attrs,
      int merged_version);

bool
wcore_config_attrs_version_le(
      const struct wcore_config_attrs *attrs,
      int merged_version);

#ifdef __cplusplus
}
#endif
