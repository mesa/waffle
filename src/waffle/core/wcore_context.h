// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdlib.h>

#include "api_object.h"

#include "wcore_config.h"
#include "wcore_util.h"

struct wcore_context;
struct wcore_display;
union waffle_native_context;

struct wcore_context {
    struct api_object api;
    enum waffle_enum context_api; // WAFFLE_CONTEXT_*
    struct wcore_display *display;
};

static inline struct waffle_context*
waffle_context(struct wcore_context *ctx) {
    return (struct waffle_context*) ctx;
}

static inline struct wcore_context*
wcore_context(struct waffle_context *ctx) {
    return (struct wcore_context*) ctx;
}

static inline void
wcore_context_init(struct wcore_context *self, struct wcore_config *config)
{
    self->api.display_id = config->display->api.display_id;
    self->context_api = config->attrs.context_api;
    self->display = config->display;
}
