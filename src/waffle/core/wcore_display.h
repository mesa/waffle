// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "c99_compat.h"
#include "threads.h"

#include "api_object.h"

#include "wcore_util.h"

#ifdef __cplusplus
extern "C" {
#endif

struct wcore_display;
struct wcore_platform;
union waffle_native_display;

struct wcore_display {
    struct api_object api;
    struct wcore_platform *platform;
};

static inline struct waffle_display*
waffle_display(struct wcore_display *dpy) {
    return (struct waffle_display*) dpy;
}

static inline struct wcore_display*
wcore_display(struct waffle_display *dpy) {
    return (struct wcore_display*) dpy;
}

void
wcore_display_init(struct wcore_display *self, struct wcore_platform *platform);

#ifdef __cplusplus
}
#endif
