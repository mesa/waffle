// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <assert.h>

#include "wcore_config.h"
#include "wcore_util.h"

struct wcore_window;
union waffle_native_window;

struct wcore_window {
    struct api_object api;
    struct wcore_display *display;
};

static inline struct waffle_window*
waffle_window(struct wcore_window *win) {
    return (struct waffle_window*) win;
}

static inline struct wcore_window*
wcore_window(struct waffle_window *win) {
    return (struct wcore_window*) win;
}

static inline void
wcore_window_init(struct wcore_window *self, struct wcore_config *config)
{

    self->api.display_id = config->display->api.display_id;
    self->display = config->display;
}
