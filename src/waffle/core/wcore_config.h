// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "api_object.h"

#include "wcore_display.h"
#include "wcore_config_attrs.h"
#include "wcore_util.h"

#ifdef __cplusplus
extern "C" {
#endif

struct wcore_config;
union waffle_native_config;

struct wcore_config {
    struct api_object api;
    struct wcore_config_attrs attrs;
    struct wcore_display *display;
};

static inline struct waffle_config*
waffle_config(struct wcore_config *cfg) {
    return (struct waffle_config*) cfg;
}

static inline struct wcore_config*
wcore_config(struct waffle_config *cfg) {
    return (struct wcore_config*) cfg;
}

static inline void
wcore_config_init(struct wcore_config *self,
                  struct wcore_display *display,
                  const struct wcore_config_attrs *attrs)
{
    self->api.display_id = display->api.display_id;
    self->display = display;
    memcpy(&self->attrs, attrs, sizeof(*attrs));
}

#ifdef __cplusplus
}
#endif
