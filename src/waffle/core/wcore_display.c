// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdio.h>

#include "wcore_display.h"

static mtx_t mutex;

static void
wcore_display_init_once(void)
{
    mtx_init(&mutex, mtx_plain);
}

void
wcore_display_init(struct wcore_display *self, struct wcore_platform *platform)
{
    static size_t id_counter = 0;
    static once_flag flag = ONCE_FLAG_INIT;

    call_once(&flag, wcore_display_init_once);
    mtx_lock(&mutex);
    self->api.display_id = ++id_counter;
    mtx_unlock(&mutex);

    self->platform = platform;

    if (self->api.display_id == 0) {
        fprintf(stderr, "waffle: error: internal counter wrapped to 0\n");
        abort();
    }
}
