// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdlib.h>

#undef linux

#include "waffle_wayland.h"

#include "wegl_platform.h"
#include "wcore_util.h"

struct wl_egl_window;
struct wl_surface;

struct wayland_platform {
    struct wegl_platform wegl;

    void *dl_wl_egl;

    struct wl_egl_window *
    (*wl_egl_window_create)(struct wl_surface *surface,
                            int width, int height);

    void
    (*wl_egl_window_destroy)(struct wl_egl_window *egl_window);

    void
    (*wl_egl_window_resize)(struct wl_egl_window *egl_window,
                            int width, int height,
                            int dx, int dy);

};

DEFINE_CONTAINER_CAST_FUNC(wayland_platform,
                           struct wayland_platform,
                           struct wegl_platform,
                           wegl)

struct wcore_platform*
wayland_platform_create(void);
