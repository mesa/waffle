// SPDX-FileCopyrightText: Copyright 2015 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wayland-util.h"
#include "wayland-version.h"

bool
wayland_wrapper_init(void);

bool
wayland_wrapper_teardown(void);


// Forward declaration of the structs required by the functions
struct wl_proxy;
struct wl_display;


// Functions
#define WAFFLE_WAYLAND_SYM(rc, fn, params) \
    typedef rc (*pfn_##fn) params; \
    extern pfn_##fn wfl_##fn;
#include "wayland_sym.h"
#undef WAFFLE_WAYLAND_SYM

#ifdef _WAYLAND_CLIENT_H
#error Do not include wayland-client.h ahead of wayland_wrapper.h
#endif

#include <wayland-client-core.h>

#define wl_display_connect (*wfl_wl_display_connect)
#define wl_display_disconnect (*wfl_wl_display_disconnect)
#define wl_display_roundtrip (*wfl_wl_display_roundtrip)
#define wl_proxy_destroy (*wfl_wl_proxy_destroy)
#define wl_proxy_add_listener (*wfl_wl_proxy_add_listener)
#define wl_proxy_set_user_data (*wfl_wl_proxy_set_user_data)
#define wl_proxy_get_user_data (*wfl_wl_proxy_get_user_data)
#define wl_proxy_get_version (*wfl_wl_proxy_get_version)
#define wl_proxy_marshal (*wfl_wl_proxy_marshal)
#define wl_proxy_marshal_constructor (*wfl_wl_proxy_marshal_constructor)
#define wl_proxy_marshal_constructor_versioned (*wfl_wl_proxy_marshal_constructor_versioned)
#if WAYLAND_VERSION_MAJOR == 1 &&                                              \
    (WAYLAND_VERSION_MINOR > 19 ||                                             \
     (WAYLAND_VERSION_MINOR == 19 && WAYLAND_VERSION_MICRO >= 91))
#define wl_proxy_marshal_flags (*wfl_wl_proxy_marshal_flags)
#define wl_proxy_marshal_array_flags (*wfl_wl_proxy_marshal_array_flags)
#endif

#include <wayland-client-protocol.h>
