// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#define WL_EGL_PLATFORM 1

#include <dlfcn.h>
#include <stdlib.h>

#include "waffle_wayland.h"

#include "wcore_error.h"

#include "posix_platform.h"

#include "wegl_config.h"
#include "wegl_context.h"
#include "wegl_platform.h"
#include "wegl_util.h"

#include "wayland_display.h"
#include "wayland_platform.h"
#include "wayland_window.h"
#include "wayland_wrapper.h"

static const char *libwl_egl_filename = "libwayland-egl.so.1";

static const struct wcore_platform_vtbl wayland_platform_vtbl;

static bool
wayland_platform_destroy(struct wcore_platform *wc_self)
{
    struct wayland_platform *self = wayland_platform(wegl_platform(wc_self));
    bool ok = true;

    if (self->dl_wl_egl) {
        if (dlclose(self->dl_wl_egl)) {
            ok &= false;
            wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                         "dlclose(\"%s\") failed: %s",
                         libwl_egl_filename, dlerror());
        }
    }

    ok &= wayland_wrapper_teardown();
    ok &= wegl_platform_teardown(&self->wegl);
    free(self);
    return ok;
}

struct wcore_platform*
wayland_platform_create(void)
{
    struct wayland_platform *self;
    bool ok = true;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    ok = wegl_platform_init(&self->wegl, EGL_PLATFORM_WAYLAND_KHR);
    if (!ok)
        goto error;

    ok = wayland_wrapper_init();
    if (!ok)
        goto error;

    self->dl_wl_egl = dlopen(libwl_egl_filename, RTLD_LAZY | RTLD_LOCAL);
    if (!self->dl_wl_egl) {
        wcore_errorf(WAFFLE_ERROR_FATAL,
                     "dlopen(\"%s\") failed: %s",
                     libwl_egl_filename, dlerror());
        goto error;
    }

#define RETRIEVE_WL_EGL_SYMBOL(function)                                  \
    self->function = dlsym(self->dl_wl_egl, #function);                \
    if (!self->function) {                                             \
        wcore_errorf(WAFFLE_ERROR_FATAL,                             \
                     "dlsym(\"%s\", \"" #function "\") failed: %s",    \
                     libwl_egl_filename, dlerror());                      \
        goto error;                                                    \
    }

    RETRIEVE_WL_EGL_SYMBOL(wl_egl_window_create);
    RETRIEVE_WL_EGL_SYMBOL(wl_egl_window_destroy);
    RETRIEVE_WL_EGL_SYMBOL(wl_egl_window_resize);

#undef RETRIEVE_WL_EGL_SYMBOL

    self->wegl.wcore.vtbl = &wayland_platform_vtbl;
    return &self->wegl.wcore;

error:
    wayland_platform_destroy(&self->wegl.wcore);
    return NULL;
}

static union waffle_native_config*
wayland_config_get_native(struct wcore_config *wc_config)
{
    struct wegl_config *config = wegl_config(wc_config);
    struct wayland_display *dpy = wayland_display(wc_config->display);
    union waffle_native_config *n_config;

    WCORE_CREATE_NATIVE_UNION(n_config, wayland);
    if (!n_config)
        return NULL;

    wayland_display_fill_native(dpy, &n_config->wayland->display);
    n_config->wayland->egl_config = config->egl;

    return n_config;
}

static union waffle_native_context*
wayland_context_get_native(struct wcore_context *wc_ctx)
{
    struct wayland_display *dpy = wayland_display(wc_ctx->display);
    struct wegl_context *ctx = wegl_context(wc_ctx);
    union waffle_native_context *n_ctx;

    WCORE_CREATE_NATIVE_UNION(n_ctx, wayland);
    if (!n_ctx)
        return NULL;

    wayland_display_fill_native(dpy, &n_ctx->wayland->display);
    n_ctx->wayland->egl_context = ctx->egl;

    return n_ctx;
}

static const struct wcore_platform_vtbl wayland_platform_vtbl = {
    .destroy = wayland_platform_destroy,

    .make_current = wegl_make_current,
    .get_proc_address = wegl_get_proc_address,
    .dl_can_open = posix_platform_can_dlopen,
    .dl_sym = posix_platform_dlsym,

    .display = {
        .connect = wayland_display_connect,
        .destroy = wayland_display_destroy,
        .supports_context_api = wegl_display_supports_context_api,
        .get_native = wayland_display_get_native,
    },

    .config = {
        .choose = wegl_config_choose,
        .destroy = wegl_config_destroy,
        .get_native = wayland_config_get_native,
    },

    .context = {
        .create = wegl_context_create,
        .destroy = wegl_context_destroy,
        .get_native = wayland_context_get_native,
    },

    .window = {
        .create = wayland_window_create,
        .destroy = wayland_window_destroy,
        .show = wayland_window_show,
        .swap_buffers = wayland_window_swap_buffers,
        .resize = wayland_window_resize,
        .get_native = wayland_window_get_native,
    },
};
