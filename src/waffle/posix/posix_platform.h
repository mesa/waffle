// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-FileCopyrightText: Copyright 2023 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

struct wcore_platform;

void
posix_platform_init(struct wcore_platform *self, const char *names[3]);

void
posix_platform_teardown(struct wcore_platform *self);

bool
posix_platform_can_dlopen(struct wcore_platform *self, int32_t waffle_dl);

void *
posix_platform_dlsym(struct wcore_platform *self,
                     int32_t waffle_dl,
                     const char *name);
