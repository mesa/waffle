// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-FileCopyrightText: Copyright 2023 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#include <assert.h>
#include <dlfcn.h>
#include <stdbool.h>
#include <stdlib.h>

#include "wcore_error.h"
#include "wcore_platform.h"
#include "wcore_util.h"

#include "posix_platform.h"

void
posix_platform_init(struct wcore_platform *self, const char *names[3])
{
    self->gl.name = names[0];
    self->gles1.name = names[1];
    self->gles2.name = names[2];
}

static void
posix_dlclose(const struct wcore_dl *self)
{
    if (!self->handle)
        return;

    if (dlclose(self->handle)) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "dlclose(libname=\"%s\") failed: %s",
                     self->name, dlerror());
    }
}

void
posix_platform_teardown(struct wcore_platform *self)
{
    // FIXME: Waffle is unable to emit a sequence of errors.
    posix_dlclose(&self->gl);
    posix_dlclose(&self->gles1);
    posix_dlclose(&self->gles2);
}

static bool
posix_dlopen(struct wcore_dl *self, const char *plat)
{
    if (self->handle)
        return true;

    if (!self->name) {
        wcore_errorf(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM,
                     "platform does not support %s", plat);
        return false;
    }

    self->handle = dlopen(self->name, RTLD_LAZY);
    if (!self->handle) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "dlopen(\"%s\") failed: %s",
                     self->name, dlerror());
        return false;
    }
    return true;
}

bool
posix_platform_can_dlopen(struct wcore_platform *self, int32_t waffle_dl)
{
    bool ret;
    WCORE_ERROR_DISABLED({
        ret = posix_dlopen(wcore_dl_from_waffle_dl(self, waffle_dl),
                           wcore_name_from_waffle_dl(waffle_dl));
    });
    return ret;
}

static void *
posix_dlsym(const struct wcore_dl *self, const char *symbol)
{
    // Clear any previous error.
    dlerror();

    void *sym = dlsym(self->handle, symbol);

    const char *error = dlerror();
    if (error) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                     "dlsym(libname=\"%s\", \"%s\") failed: %s", self->name,
                     symbol, error);
        return NULL;
    }

    return sym;
}

void *
posix_platform_dlsym(struct wcore_platform *self,
                     int32_t waffle_dl,
                     const char *name)
{
    bool ret = posix_dlopen(wcore_dl_from_waffle_dl(self, waffle_dl),
                            wcore_name_from_waffle_dl(waffle_dl));
    if (!ret)
        return NULL;

    return posix_dlsym(wcore_dl_from_waffle_dl(self, waffle_dl), name);
}
