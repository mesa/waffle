// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <assert.h>

#include "wcore_error.h"

#include "x11_display.h"
#include "x11_wrappers.h"

bool
x11_display_init(struct x11_display *self, const char *name)
{
    self->xlib = wrapped_XOpenDisplay(name);
    if (!self->xlib) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "XOpenDisplay failed");
        return false;
    }

    self->xcb = wrapped_XGetXCBConnection(self->xlib);
    if (!self->xcb) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "XGetXCBConnection failed");
        wrapped_XCloseDisplay(self->xlib);
        return false;
    }

    self->screen = DefaultScreen(self->xlib);

    return true;
}

bool
x11_display_teardown(struct x11_display *self)
{
    if (!self->xlib)
        return 1;

    int error = wrapped_XCloseDisplay(self->xlib);
    if (error)
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "XCloseDisplay failed");

    return !error;
}
