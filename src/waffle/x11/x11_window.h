// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include <X11/Xlib-xcb.h>

struct x11_display;

struct x11_window {
    struct x11_display *display;
    xcb_window_t xcb;
};

bool
x11_window_init(struct x11_window *self,
                struct x11_display *dpy,
                xcb_visualid_t visual_id,
                int32_t width,
                int32_t height);

bool
x11_window_teardown(struct x11_window *self);

bool
x11_window_show(struct x11_window *self);

bool
x11_window_resize(struct x11_window *self, int32_t width, int32_t height);
