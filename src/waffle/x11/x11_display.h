// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include <X11/Xlib-xcb.h>

struct x11_display {
    Display *xlib;
    xcb_connection_t *xcb;
    int screen;
};

bool
x11_display_init(struct x11_display *self, const char *name);

bool
x11_display_teardown(struct x11_display *self);
