// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wegl_surface.h"

struct wcore_platform;

struct droid_window {
    /// Used by droid_surfaceflinger.cpp.
    struct droid_ANativeWindow_container *pANWContainer;
    struct wegl_surface wegl;
};

static inline struct droid_window*
droid_window(struct wcore_window *wc_self)
{
    if (wc_self) {
        struct wegl_surface *wegl_self = container_of(wc_self, struct wegl_surface, wcore);
        return container_of(wegl_self, struct droid_window, wegl);
    }
    else {
        return NULL;
    }
}
struct wcore_window*
droid_window_create(struct wcore_platform *wc_plat,
                    struct wcore_config *wc_config,
                    int32_t width,
                    int32_t height,
                    const intptr_t attrib_list[]);

bool
droid_window_destroy(struct wcore_window *wc_self);

bool
droid_window_show(struct wcore_window *wc_self);

bool
droid_window_resize(struct wcore_window *wc_self,
                  int32_t width,
                  int32_t height);
