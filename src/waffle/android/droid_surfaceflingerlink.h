// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

struct droid_surfaceflinger_container;
struct droid_ANativeWindow_container;

void
droid_destroy_surface(
    struct droid_surfaceflinger_container* pSFContainer,
    struct droid_ANativeWindow_container* pANWContainer);

bool
droid_show_surface(
    struct droid_surfaceflinger_container* pSFContainer,
    struct droid_ANativeWindow_container* pANWContainer);

bool
droid_resize_surface(
    struct droid_surfaceflinger_container* pSFContainer,
    struct droid_ANativeWindow_container* pANWContainer,
    int width,
    int height);

struct droid_ANativeWindow_container*
droid_create_surface(
    int width,
    int height,
    struct droid_surfaceflinger_container* pSFContainer);

bool
droid_deinit_gl(struct droid_surfaceflinger_container* pSFContainer);

struct droid_surfaceflinger_container*
droid_init_gl();

#ifdef __cplusplus
};
#endif
