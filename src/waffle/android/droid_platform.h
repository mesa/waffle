// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_platform.h"
#include "wcore_util.h"

struct droid_platform {
    struct wcore_platform wcore;
};

DEFINE_CONTAINER_CAST_FUNC(droid_platform,
                           struct droid_platform,
                           struct wcore_platform,
                           wcore)

struct wcore_platform*
droid_platform_create(void);
