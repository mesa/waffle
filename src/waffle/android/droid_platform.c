// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdlib.h>

#include "wcore_error.h"

#include "posix_platform.h"

#include "wegl_config.h"
#include "wegl_context.h"
#include "wegl_util.h"

#include "droid_display.h"
#include "droid_platform.h"
#include "droid_window.h"

static const struct wcore_platform_vtbl droid_platform_vtbl;

static bool
droid_platform_destroy(struct wcore_platform *wc_self)
{
    struct droid_platform *self = droid_platform(wc_self);

    posix_platform_teardown(wc_self);
    free(self);
    return true;
}

struct wcore_platform*
droid_platform_create(void)
{
    static const char *dso_names[] = {
        NULL,
        "libGLESv1_CM.so",
        "libGLESv2.so",
    };
    struct droid_platform *self;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    wcore_platform_init(&self->wcore);
    posix_platform_init(&self->wcore, dso_names);

    self->wcore.vtbl = &droid_platform_vtbl;
    return &self->wcore;
}

static const struct wcore_platform_vtbl droid_platform_vtbl = {
    .destroy = droid_platform_destroy,

    .make_current = wegl_make_current,
    .get_proc_address = wegl_get_proc_address,
    .dl_can_open = posix_platform_can_dlopen,
    .dl_sym = posix_platform_dlsym,

    .display = {
        .connect = droid_display_connect,
        .destroy = droid_display_disconnect,
        .supports_context_api = wegl_display_supports_context_api,
        .get_native = NULL,
    },

    .config = {
        .choose = wegl_config_choose,
        .destroy = wegl_config_destroy,
        .get_native = NULL,
    },

    .context = {
        .create = wegl_context_create,
        .destroy = wegl_context_destroy,
        .get_native = NULL,
    },

    .window = {
        .create = droid_window_create,
        .destroy = droid_window_destroy,
        .show = droid_window_show,
        .swap_buffers = wegl_surface_swap_buffers,
        .resize = droid_window_resize,
        .get_native = NULL,
    },
};
