// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdlib.h>

#include "wcore_error.h"

#include "droid_display.h"
#include "droid_platform.h"
#include "droid_surfaceflingerlink.h"

struct wcore_display*
droid_display_connect(struct wcore_platform *wc_plat,
                        const char *name)
{
    bool ok = true;
    struct droid_display *self;

    (void) name;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    self->pSFContainer = droid_init_gl();

    if (self->pSFContainer == NULL)
        goto error;

    ok = wegl_display_init(&self->wegl, wc_plat, EGL_DEFAULT_DISPLAY);
    if (!ok)
        goto error;

    return &self->wegl.wcore;

error:
    droid_display_disconnect(&self->wegl.wcore);
    return NULL;
}

bool
droid_display_disconnect(struct wcore_display *wc_self)
{
    struct droid_display *self = droid_display(wc_self);
    bool ok = true;

    if (self->pSFContainer)
        droid_deinit_gl(self->pSFContainer);

    ok &= wegl_display_teardown(&self->wegl);
    free(self);
    return ok;
}
