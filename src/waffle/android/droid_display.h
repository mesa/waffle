// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "wegl_display.h"

struct droid_surfaceflinger_container;

struct droid_display {
    /// Used by droid_surfaceflinger.cpp.
    struct droid_surfaceflinger_container *pSFContainer;
    struct wegl_display wegl;
};

static inline struct droid_display*
droid_display(struct wcore_display *wc_self)
{
    if (wc_self) {
        struct wegl_display *wegl_self = container_of(wc_self, struct wegl_display, wcore);
        return container_of(wegl_self, struct droid_display, wegl);
    }
    else {
        return NULL;
    }
}

struct wcore_display*
droid_display_connect(struct wcore_platform *wc_plat,
                      const char *name);

bool
droid_display_disconnect(struct wcore_display *wc_self);
