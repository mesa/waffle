// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdlib.h>
#include <string.h>

#include "wcore_attrib_list.h"
#include "wcore_error.h"

#include "wegl_config.h"

#include "droid_display.h"
#include "droid_surfaceflingerlink.h"
#include "droid_window.h"

struct wcore_window*
droid_window_create(struct wcore_platform *wc_plat,
                    struct wcore_config *wc_config,
                    int32_t width,
                    int32_t height,
                    const intptr_t attrib_list[])
{
    struct droid_window *self;
    struct wegl_config *config = wegl_config(wc_config);
    struct droid_display *dpy = droid_display(wc_config->display);
    bool ok = true;

    (void) wc_plat;

    if (width == -1 && height == -1) {
        wcore_errorf(WAFFLE_ERROR_UNSUPPORTED_ON_PLATFORM,
                     "fullscreen window not supported");
        return NULL;
    }

    if (wcore_attrib_list_length(attrib_list) > 0) {
        wcore_error_bad_attribute(attrib_list[0]);
    }

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    self->pANWContainer = droid_create_surface(width, height,
                                               dpy->pSFContainer);
    if (!self->pANWContainer)
        goto error_droid_create_surface;

    ok = wegl_window_init(&self->wegl, wc_config,
                          (intptr_t) *((intptr_t*)(self->pANWContainer)));
    if (!ok)
        goto error;

    return &self->wegl.wcore;

error:
    droid_window_destroy(&self->wegl.wcore);
error_droid_create_surface:
    return NULL;
}

bool
droid_window_destroy(struct wcore_window *wc_self)
{
    struct droid_window *self = droid_window(wc_self);
    struct droid_display *dpy;
    bool ok = true;

    dpy = droid_display(self->wegl.wcore.display);

    ok &= wegl_surface_teardown(&self->wegl);
    droid_destroy_surface(dpy->pSFContainer, self->pANWContainer);
    free(self);
    return ok;
}

bool
droid_window_show(struct wcore_window *wc_self)
{
    struct droid_window *self = droid_window(wc_self);
    struct droid_display *dpy;

    dpy = droid_display(wc_self->display);

    return droid_show_surface(dpy->pSFContainer, self->pANWContainer);
}

bool
droid_window_resize(struct wcore_window *wc_self,
                    int32_t width,
                    int32_t height)
{
    struct droid_window *self = droid_window(wc_self);
    struct droid_display *dpy;

    dpy = droid_display(wc_self->display);

    return droid_resize_surface(dpy->pSFContainer, self->pANWContainer,
        width, height);
}
