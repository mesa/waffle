// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "waffle_x11_egl.h"

#include "x11_display.h"

#include "wegl_display.h"

struct wcore_platform;

struct xegl_display {
    struct x11_display x11;
    struct wegl_display wegl;
};

static inline struct xegl_display*
xegl_display(struct wcore_display *wc_self)
{
    if (wc_self) {
        struct wegl_display *wegl_self = container_of(wc_self, struct wegl_display, wcore);
        return container_of(wegl_self, struct xegl_display, wegl);
    }
    else {
        return NULL;
    }
}

struct wcore_display*
xegl_display_connect(struct wcore_platform *wc_plat,
                     const char *name);

bool
xegl_display_destroy(struct wcore_display *wc_self);

void
xegl_display_fill_native(struct xegl_display *self,
                         struct waffle_x11_egl_display *n_dpy);

union waffle_native_display*
xegl_display_get_native(struct wcore_display *wc_self);
