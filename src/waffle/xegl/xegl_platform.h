// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#undef linux

#include "waffle_x11_egl.h"

#include "wegl_platform.h"
#include "wcore_util.h"

struct xegl_platform {
    struct wegl_platform wegl;
};

DEFINE_CONTAINER_CAST_FUNC(xegl_platform,
                           struct xegl_platform,
                           struct wegl_platform,
                           wegl)

struct wcore_platform*
xegl_platform_create(void);
