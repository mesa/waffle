// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_window.h"
#include "wcore_util.h"

#include "x11_window.h"

#include "wegl_surface.h"

struct wcore_platform;

struct xegl_window {
    struct x11_window x11;
    struct wegl_surface wegl;
};

static inline struct xegl_window*
xegl_window(struct wcore_window *wc_self)
{
    if (wc_self) {
        struct wegl_surface *wegl_self = container_of(wc_self, struct wegl_surface, wcore);
        return container_of(wegl_self, struct xegl_window, wegl);
    }
    else {
        return NULL;
    }
}

struct wcore_window*
xegl_window_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   int32_t width,
                   int32_t height,
                   const intptr_t attrib_list[]);

bool
xegl_window_destroy(struct wcore_window *wc_self);

bool
xegl_window_show(struct wcore_window *wc_self);

bool
xegl_window_resize(struct wcore_window *wc_self,
                   int32_t width, int32_t height);

union waffle_native_window*
xegl_window_get_native(struct wcore_window *wc_self);
