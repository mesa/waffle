// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#include <stdlib.h>

#include "wcore_error.h"
#include "wcore_display.h"

#include "xegl_display.h"
#include "xegl_platform.h"

bool
xegl_display_destroy(struct wcore_display *wc_self)
{
    struct xegl_display *self = xegl_display(wc_self);
    bool ok = wegl_display_teardown(&self->wegl);

    ok &= x11_display_teardown(&self->x11);
    free(self);
    return ok;
}

struct wcore_display*
xegl_display_connect(
        struct wcore_platform *wc_plat,
        const char *name)
{
    struct xegl_display *self;
    bool ok = true;

    self = wcore_calloc(sizeof(*self));
    if (self == NULL)
        return NULL;

    ok = x11_display_init(&self->x11, name);
    if (!ok)
        goto error;

    ok = wegl_display_init(&self->wegl, wc_plat, self->x11.xlib);
    if (!ok)
        goto error;

    return &self->wegl.wcore;

error:
    xegl_display_destroy(&self->wegl.wcore);
    return NULL;
}

void
xegl_display_fill_native(struct xegl_display *self,
                         struct waffle_x11_egl_display *n_dpy)
{
    n_dpy->xlib_display = self->x11.xlib;
    n_dpy->egl_display = self->wegl.egl;
}

union waffle_native_display*
xegl_display_get_native(struct wcore_display *wc_self)
{
    struct xegl_display *self = xegl_display(wc_self);
    union waffle_native_display *n_dpy;

    WCORE_CREATE_NATIVE_UNION(n_dpy, x11_egl);
    if (!n_dpy)
        return NULL;

    xegl_display_fill_native(self, n_dpy->x11_egl);
    return n_dpy;
}
