// SPDX-FileCopyrightText: Copyright 2014-2023 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

struct wcore_platform;

void
win_platform_init(struct wcore_platform *wc_plat);

void
win_platform_teardown(struct wcore_platform *wc_plat);

bool
win_platform_can_dlopen(struct wcore_platform *wc_plat, int32_t waffle_dl);

void *
win_platform_dlsym(struct wcore_platform *wc_plat,
                   int32_t waffle_dl,
                   const char *name);
