// SPDX-FileCopyrightText: Copyright 2014-2023 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#include <windows.h>

#include "wcore_error.h"
#include "wcore_platform.h"

#include "win_platform.h"

void
win_platform_init(struct wcore_platform *self)
{
    self->gl.name = "OPENGL32";
}

static void
win_dlclose(struct wcore_dl *self)
{
    if (!FreeLibrary(self->handle)) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                     "FreeLibrary(libname=\"%s\") failed: %ld", self->name,
                     GetLastError());
    }
}

void
win_platform_teardown(struct wcore_platform *self)
{
    win_dlclose(&self->gl);
}

static bool
win_dlopen(struct wcore_dl *self)
{
    if (self->handle)
        return true;

    self->handle = LoadLibraryA(self->name);
    if (!self->handle) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN, "LoadLibraryA(\"%s\") failed: %ld",
                     self->name, GetLastError());
        return false;
    }

    return true;
}

bool
win_platform_can_dlopen(struct wcore_platform *self, int32_t waffle_dl)
{
    bool ret;
    WCORE_ERROR_DISABLED({ ret = win_dlopen(&self->gl); });

    return ret;
}

static void *
win_dlsym(const struct wcore_dl *self, const char *symbol)
{
    // Clear any previous error.
    GetLastError();

    void *sym = GetProcAddress(self->handle, symbol);
    if (!sym) {
        wcore_errorf(WAFFLE_ERROR_UNKNOWN,
                     "GetProcAddress(libname=\"%s\", \"%s\") failed: %ld",
                     self->name, symbol, GetLastError());
        return NULL;
    }

    return sym;
}

void *
win_platform_dlsym(struct wcore_platform *self,
                   int32_t waffle_dl,
                   const char *name)
{
    // OPENGL32.DLL provides GL and GLES* symbols.
    bool ret = win_dlopen(&self->gl);
    if (!ret)
        return NULL;

    return win_dlsym(&self->gl, name);
}
