// SPDX-FileCopyrightText: Copyright 2014 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "wcore_platform.h"
#include "wcore_util.h"

struct wgl_platform {
    struct wcore_platform wcore;

    /// @brief The full system version number - (major << 8) | minor.
    ///
    /// For example, 0x0501 indicates Windows XP.
    int32_t system_version_full;

    /// @brief The class name of the waffle windows.
    const char *class_name;
};

static inline struct wgl_platform*
wgl_platform(struct wcore_platform *wcore)
{
    return (struct wgl_platform*)wcore;
}

struct wcore_platform*
wgl_platform_create(void);
