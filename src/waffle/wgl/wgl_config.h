// SPDX-FileCopyrightText: Copyright 2014 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <GL/gl.h>
#include <GL/wglext.h>

#include "wcore_config.h"
#include "wcore_util.h"

struct wcore_config_attrs;
struct wcore_platform;
struct wgl_window;

struct wgl_config {
    struct wcore_config wcore;
    PIXELFORMATDESCRIPTOR pfd;
    int pixel_format;

    // XXX: Currently we manage only one window per config.
    struct wgl_window *window;
};

static inline struct wgl_config*
wgl_config(struct wcore_config *wcore)
{
    return (struct wgl_config *)wcore;
}

struct wcore_config*
wgl_config_choose(struct wcore_platform *wc_plat,
                  struct wcore_display *wc_dpy,
                  const struct wcore_config_attrs *attrs);

bool
wgl_config_destroy(struct wcore_config *wc_self);
