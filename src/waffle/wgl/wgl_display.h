// SPDX-FileCopyrightText: Copyright 2014 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "wcore_display.h"
#include "wcore_util.h"

struct wcore_platform;

// XXX: Move the typedefs ?
typedef HGLRC (__stdcall *PFNWGLCREATECONTEXTATTRIBSARBPROC)(HDC hDC,
                                                             HGLRC hShareContext,
                                                             const int *attribList);

typedef BOOL (__stdcall *PFNWGLCHOOSEPIXELFORMATARBPROC )(HDC hdc,
                                                          const int * piAttribIList,
                                                          const float * pfAttribFList,
                                                          unsigned int nMaxFormats,
                                                          int * piFormats,
                                                          unsigned int * nNumFormats);

struct wgl_display {
    struct wcore_display wcore;

    HWND hWnd;
    HDC hDC;
    int pixel_format;
    HGLRC hglrc;

    bool ARB_create_context;
    bool ARB_create_context_profile;
    bool ARB_create_context_robustness;
    bool EXT_create_context_es_profile;
    bool EXT_create_context_es2_profile;
    bool ARB_pixel_format;

    PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
    PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
};

static inline struct wgl_display*
wgl_display(struct wcore_display *wcore)
{
    return (struct wgl_display*)wcore;
}

struct wcore_display*
wgl_display_connect(struct wcore_platform *wc_plat,
                    const char *name);

bool
wgl_display_destroy(struct wcore_display *wc_self);

bool
wgl_display_supports_context_api(struct wcore_display *wc_self,
                                 int32_t context_api);
