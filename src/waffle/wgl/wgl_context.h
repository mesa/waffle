// SPDX-FileCopyrightText: Copyright 2014 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_config_attrs.h"
#include "wcore_context.h"
#include "wcore_util.h"

struct wcore_config;
struct wcore_platform;

struct wgl_context {
    struct wcore_context wcore;
    HGLRC hglrc;
};

static inline struct wgl_context*
wgl_context(struct wcore_context *wcore)
{
    return (struct wgl_context *)wcore;
}

struct wcore_context*
wgl_context_create(struct wcore_platform *wc_plat,
                   struct wcore_config *wc_config,
                   struct wcore_context *wc_share_ctx);

bool
wgl_context_destroy(struct wcore_context *wc_self);

// XXX: Keep in sync with wgl_config_check_context_attrs()
static inline bool
wgl_context_needs_arb_create_context(const struct wcore_config_attrs *attrs)
{
    // Any of the following require the ARB extension, since the data is
    // passed as attributes.

    // Using any GLES* profile,
    if (attrs->context_api != WAFFLE_CONTEXT_OPENGL)
        return true;

    // explicitly requesting OpenGL version (>=3.2),
    if (wcore_config_attrs_version_ge(attrs, 32))
        return true;

    // ... or any of the context flags.
    if (attrs->context_forward_compatible)
        return true;

    if (attrs->context_debug)
        return true;

    if (attrs->context_robust)
        return true;

    return false;
}
