// SPDX-FileCopyrightText: Copyright 2014 Emil Velikov
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>

#include "wcore_util.h"
#include "wcore_window.h"

struct wcore_platform;

struct wgl_window {
    struct wcore_window wcore;
    HWND hWnd;
    HDC hDC;
    bool created;
};

static inline struct wgl_window*
wgl_window(struct wcore_window *wcore)
{
    return (struct wgl_window*)wcore;
}

struct wcore_window*
wgl_window_priv_create(struct wcore_platform *wc_plat,
                       struct wcore_config *wc_config,
                       int32_t width,
                       int32_t height);

bool
wgl_window_priv_destroy(struct wcore_window *wc_self);

struct wcore_window*
wgl_window_create(struct wcore_platform *wc_plat,
                  struct wcore_config *wc_config,
                  int32_t width,
                  int32_t height,
                  const intptr_t attrib_list[]);

bool
wgl_window_destroy(struct wcore_window *wc_self);

bool
wgl_window_show(struct wcore_window *wc_self);

bool
wgl_window_resize(struct wcore_window *wc_self,
                  int32_t width, int32_t height);

bool
wgl_window_swap_buffers(struct wcore_window *wc_self);

union waffle_native_window*
wgl_window_get_native(struct wcore_window *wc_self);
