# SPDX-FileCopyrightText: Copyright © 2018 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

files_man_common = files(
  'common/author-lina.versace.xml',
  'common/author-emil.velikov.xml',
  'common/author-jordan.l.justen.xml',
  'common/copyright.xml',
  'common/error-codes.xml',
  'common/issues.xml',
  'common/legalnotice.xml',
  'common/return-value.xml',
)

# Each entry is a 4 part tuple consisting of
# <man section>, <name>, <extra pages that are <name>_<page>>, <extra_pages>
man_targets = [
  ['1', 'wflinfo', [], []],
  ['3', 'waffle_attrib_list', ['get', 'get_with_default', 'length', 'update'], []],
  ['3', 'waffle_config', ['choose', 'destroy', 'get_native'], []],
  ['3', 'waffle_context', ['create', 'destroy', 'get_native'], []],
  ['3', 'waffle_display', ['connect', 'disconnect', 'get_native', 'supports_context_api'], []],
  ['3', 'waffle_dl', ['can_open', 'sym'], []],
  ['3', 'waffle_enum', ['to_string'], []],
  ['3', 'waffle_error', ['get_code', 'get_info', 'to_string'], []],
  ['3', 'waffle_gbm', ['config', 'context', 'display', 'window'], []],
  ['3', 'waffle_get_proc_address', [], []],
  ['3', 'waffle_glx', ['config', 'context', 'display', 'window'], []],
  ['3', 'waffle_init', [], []],
  ['3', 'waffle_is_extension_in_string', [], []],
  ['3', 'waffle_make_current', [], ['waffle_get_current_display', 'waffle_get_current_window', 'waffle_get_current_context']],
  ['3', 'waffle_native', ['config', 'context', 'display', 'window'], []],
  ['3', 'waffle_teardown', [], []],
  ['3', 'waffle_wayland', ['config', 'context', 'display', 'window'], []],
  ['3', 'waffle_window', ['create', 'destroy', 'get_native', 'show', 'swap_buffers'], []],
  ['3', 'waffle_x11_egl', ['config', 'context', 'display', 'window'], []],
  ['7', 'waffle', [], []],
  ['7', 'waffle_feature_test_macros', [], []],
]


if get_option('build-manpages')
  prog_xslt = find_program('xsltproc')
  mans = []

  foreach s : man_targets
    extra_out = []
    foreach e : s[2]
      extra_out += '@0@_@1@.@2@'.format(s[1], e, s[0])
    endforeach
    foreach e : s[3]
      extra_out += '@0@.@1@'.format(e, s[0])
    endforeach

    name = '@0@.@1@'.format(s[1], s[0])

    mans += custom_target(
      name,
      input : ['manpage.xsl', name + '.xml'],
      output : [name, extra_out],
      command : [prog_xslt, '-nonet', '--xinclude', '-o', '@OUTPUT0@', '@INPUT@'],
      install : true,
      install_dir : join_paths(get_option('mandir'), 'man' + s[0]),
      depend_files : files_man_common,
    )
  endforeach
endif

if get_option('build-htmldocs')
  prog_xslt = find_program('xsltproc')

  foreach s : man_targets
    extra_out = []
    foreach e : s[2]
      extra_out += '@0@_@1@.@2@'.format(s[1], e, s[0])
    endforeach
    foreach e : s[3]
      extra_out += '@0@.@1@'.format(e, s[0])
    endforeach

    name = '@0@.@1@'.format(s[1], s[0])

    custom_target(
      name + '.html',
      input : ['html.xsl', name + '.xml'],
      output : [name + '.html'],
      command : [prog_xslt, '-nonet', '--xinclude', '-o', '@OUTPUT0@', '@INPUT@'],
      install : true,
      install_dir : join_paths(docdir, 'html', 'man'),
      depend_files : files_man_common,
    )
  endforeach
endif
