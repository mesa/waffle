# SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
# SPDX-License-Identifier: BSD-2-Clause

set(man_out_dir ${CMAKE_CURRENT_BINARY_DIR})

file(MAKE_DIRECTORY ${man_out_dir}/man1)
file(MAKE_DIRECTORY ${man_out_dir}/man3)
file(MAKE_DIRECTORY ${man_out_dir}/man7)

set(man_outputs
    ${man_out_dir}/man1/wflinfo.1
    ${man_out_dir}/man3/waffle_attrib_list.3
    ${man_out_dir}/man3/waffle_config.3
    ${man_out_dir}/man3/waffle_context.3
    ${man_out_dir}/man3/waffle_display.3
    ${man_out_dir}/man3/waffle_dl.3
    ${man_out_dir}/man3/waffle_enum.3
    ${man_out_dir}/man3/waffle_error.3
    ${man_out_dir}/man3/waffle_gbm.3
    ${man_out_dir}/man3/waffle_get_proc_address.3
    ${man_out_dir}/man3/waffle_glx.3
    ${man_out_dir}/man3/waffle_init.3
    ${man_out_dir}/man3/waffle_is_extension_in_string.3
    ${man_out_dir}/man3/waffle_make_current.3
    ${man_out_dir}/man3/waffle_native.3
    ${man_out_dir}/man3/waffle_teardown.3
    ${man_out_dir}/man3/waffle_wayland.3
    ${man_out_dir}/man3/waffle_window.3
    ${man_out_dir}/man3/waffle_x11_egl.3
    ${man_out_dir}/man7/waffle.7
    ${man_out_dir}/man7/waffle_feature_test_macros.7
    )

set(man_common_sources
    ${common_sources}
    manpage.xsl
    )

function(waffle_add_manpage vol title)
    add_custom_command(
        OUTPUT man${vol}/${title}.${vol}
        DEPENDS ${title}.${vol}.xml ${man_common_sources}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMAND ${waffle_xsltproc} -nonet --xinclude -o ${man_out_dir}/man${vol}/ manpage.xsl ${title}.${vol}.xml
        )
endfunction()

waffle_add_manpage(1 wflinfo)
waffle_add_manpage(3 waffle_attrib_list)
waffle_add_manpage(3 waffle_config)
waffle_add_manpage(3 waffle_context)
waffle_add_manpage(3 waffle_display)
waffle_add_manpage(3 waffle_dl)
waffle_add_manpage(3 waffle_enum)
waffle_add_manpage(3 waffle_error)
waffle_add_manpage(3 waffle_gbm)
waffle_add_manpage(3 waffle_get_proc_address)
waffle_add_manpage(3 waffle_glx)
waffle_add_manpage(3 waffle_init)
waffle_add_manpage(3 waffle_is_extension_in_string)
waffle_add_manpage(3 waffle_make_current)
waffle_add_manpage(3 waffle_native)
waffle_add_manpage(3 waffle_teardown)
waffle_add_manpage(3 waffle_wayland)
waffle_add_manpage(3 waffle_window)
waffle_add_manpage(3 waffle_x11_egl)
waffle_add_manpage(7 waffle)
waffle_add_manpage(7 waffle_feature_test_macros)

add_custom_target(man
    ALL
    DEPENDS ${man_outputs}
    )

install(
    DIRECTORY ${man_out_dir}/man1
    DESTINATION ${CMAKE_INSTALL_MANDIR}
    COMPONENT manuals
    )
install(
    DIRECTORY ${man_out_dir}/man3
    DESTINATION ${CMAKE_INSTALL_MANDIR}
    COMPONENT manuals
    )
install(
    DIRECTORY ${man_out_dir}/man7
    DESTINATION ${CMAKE_INSTALL_MANDIR}
    COMPONENT manuals
    )
