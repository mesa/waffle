// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>
#include <X11/Xlib.h>

#ifdef __cplusplus
extern "C" {
#endif

struct waffle_x11_egl_display {
    Display *xlib_display;
    EGLDisplay egl_display;
};

struct waffle_x11_egl_config {
    struct waffle_x11_egl_display display;
    EGLConfig egl_config;
};

struct waffle_x11_egl_context {
    struct waffle_x11_egl_display display;
    EGLContext egl_context;
};

struct waffle_x11_egl_window {
    struct waffle_x11_egl_display display;
    XID xlib_window;
    EGLSurface egl_surface;
};

#ifdef __cplusplus
} // end extern "C"
#endif
