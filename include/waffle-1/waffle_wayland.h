// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#define WL_EGL_PLATFORM 1

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#include "waffle.h"

#ifdef __cplusplus
extern "C" {
#endif

struct wl_compositor;
struct wl_display;
struct wl_egl_window;
struct wl_shell;
struct wl_shell_surface;
struct xdg_wm_base;
struct wl_surface;

struct waffle_wayland_display {
    struct wl_display *wl_display;
    struct wl_compositor *wl_compositor;
    // Will be NULL when compositor does not support the old wl_shell protocol
    struct wl_shell *wl_shell WAFFLE_DEPRECATED_1_07;
    EGLDisplay egl_display;
#if WAFFLE_API_VERSION >= 0x0107
    // Will be NULL when compositor does not support the new xdg-shell protocol
    struct xdg_wm_base *xdg_shell;
#endif
};

struct waffle_wayland_config {
    struct waffle_wayland_display display;
    EGLConfig egl_config;
};

struct waffle_wayland_context {
    struct waffle_wayland_display display;
    EGLContext egl_context;
};

struct waffle_wayland_window {
    struct waffle_wayland_display display;
    struct wl_surface *wl_surface;
    // Will be NULL when compositor does not support the old wl_shell protocol
    struct wl_shell_surface *wl_shell_surface WAFFLE_DEPRECATED_1_07;
    struct wl_egl_window *wl_window;
    EGLSurface egl_surface;
#if WAFFLE_API_VERSION >= 0x0107
    // Will be NULL when compositor does not support the new xdg-shell protocol
    struct xdg_surface *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;
#endif
};

#ifdef __cplusplus
} // end extern "C"
#endif
