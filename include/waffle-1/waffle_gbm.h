// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#define __GBM__ 1

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#ifdef __cplusplus
extern "C" {
#endif

struct gbm_device;
struct gbm_surface;

struct waffle_gbm_display {
    struct gbm_device *gbm_device;
    EGLDisplay egl_display;
};

struct waffle_gbm_config {
    struct waffle_gbm_display display;
    EGLConfig egl_config;
};

struct waffle_gbm_context {
    struct waffle_gbm_display display;
    EGLContext egl_context;
};

struct waffle_gbm_window {
    struct waffle_gbm_display display;
    struct gbm_surface *gbm_surface;
    EGLSurface egl_surface;
};

#ifdef __cplusplus
} // end extern "C"
#endif
