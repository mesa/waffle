// SPDX-FileCopyrightText: Copyright 2022 Google
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <EGL/egl.h>

#ifdef __cplusplus
extern "C" {
#endif

struct waffle_surfaceless_egl_display {
    EGLDisplay egl_display;
};

struct waffle_surfaceless_egl_config {
    struct waffle_surfaceless_egl_display display;
    EGLConfig egl_config;
};

struct waffle_surfaceless_egl_context {
    struct waffle_surfaceless_egl_display display;
    EGLContext egl_context;
};

struct waffle_surfaceless_egl_window {
    struct waffle_surfaceless_egl_display display;
    EGLSurface egl_surface;
};

#ifdef __cplusplus
} // end extern "C"
#endif
