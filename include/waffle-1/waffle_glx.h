// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <GL/glx.h>

#ifdef __cplusplus
extern "C" {
#endif

struct waffle_glx_display {
    Display *xlib_display;
};

struct waffle_glx_config {
    Display *xlib_display;
    GLXFBConfig glx_fbconfig;
};

struct waffle_glx_context {
    Display *xlib_display;
    GLXContext glx_context;
};

struct waffle_glx_window {
    Display *xlib_display;
    XID xlib_window;
};

#ifdef __cplusplus
} // end extern "C"
#endif
