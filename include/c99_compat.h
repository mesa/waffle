// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

/*
 * C99 restrict keyword
 */
#ifndef restrict
#  if defined(__GNUC__)
#    define restrict __restrict__
#  elif defined(_MSC_VER)
     /* leave empty to prevent compiler wows - decl vs. def diff */
#    define restrict
#  else
#    define restrict
#  endif
#endif

/*
 * C99 inline keyword
 */
#ifndef inline
#  if defined(__GNUC__)
#    define inline __inline__
#  elif defined(_MSC_VER)
#    define inline __inline
#  elif defined(__ICL)
#    define inline __inline
#  else
#    define inline
#  endif
#endif

/*
 * C99 string manipulation functions - snprintf, strcasecmp
 *
 * https://stackoverflow.com/questions/2915672/snprintf-and-visual-studio-2010
 */
#if defined(_MSC_VER)
#include <stdarg.h> // for va_start, va_end
#include <stdio.h>  // for _vscprintf, _vscprintf_s
#include <string.h>

#define strcasecmp _stricmp
#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf

static inline int
c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)
{
    int count = -1;

    if (size != 0)
        count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);
    if (count == -1)
        count = _vscprintf(format, ap);

    return count;
}

static inline int
c99_snprintf(char* str, size_t size, const char* format, ...)
{
    int count;
    va_list ap;

    va_start(ap, format);
    count = c99_vsnprintf(str, size, format, ap);
    va_end(ap);

    return count;
}

#else
#include <strings.h>

#endif

/*
 * strerror_r - strictly speaking not C99 function, but it's been around of
 * ages in one shape or another.
 *
 * Under *NIX there are three flavours - XSI-compliant (POSIX.1-2001)
 * or not, vs GNU-specific one. The POSIX one is available only under *NIX
 * and is guaranteed to be thread-safe (as used in waffle).
 * Under Windows the function is not thread-safe and thus it's depreciated.
 */
#if defined(_WIN32)
#define strerror_r(errno,buf,len) strerror_s(buf,len,errno)
#endif
