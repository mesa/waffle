# SPDX-License-Identifier: BSD-2-Clause

# globally disable file completions
complete -c wflinfo -f

# first two are required, the rest optional...
set -l platforms 'android cgl gbm glx surfaceless_egl sl wayland wgl x11_egl'
complete -c wflinfo -s p -l platform -d 'Platform type' -x -ra "$platforms"
complete -c wflinfo -s a -l api -d 'API type' -x -ra 'gl gles1 gles2 gles3'

set -l apis "1.0 1.1 1.2 1.3 1.4 1.5 2.0 2.1 3.0 3.1 3.2 3.3 4.0 4.1 4.2 4.3 4.4 4.5 4.6"
complete -c wflinfo -s V -l version -d 'API version' -x -ra "$apis"
complete -c wflinfo      -l profile -d 'OpenGL profile' -x -ra 'core compat none'
complete -c wflinfo -s v -l verbose -d 'Print more information'
complete -c wflinfo      -l forward-compatible -d 'Create a forward-compatible context'
complete -c wflinfo      -l debug-context -d 'Create a debug context'
complete -c wflinfo      -l robust-context -d 'Create a robust context'
complete -c wflinfo      -l reset-notification -d 'Create a context with reset notification'
complete -c wflinfo -s f -l format -d 'Output format, default original' -x -ra 'original json'
complete -c wflinfo -s h -l help -d 'Print wflinfo usage information'
