// SPDX-FileCopyrightText: Copyright 2012 Intel Corporation
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

void
gl_basic_cocoa_init(void);

void
gl_basic_cocoa_finish(void);
